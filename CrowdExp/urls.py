"""CrowdExp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import experiment_creator.views.student

urlpatterns = [
    url(r'^$', experiment_creator.views.student.dashboard, name='dashboard'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^experiments/', include('experiment_creator.urls')),
    url(r'^runner/', include('experiment_runner.urls')),
    url(r'^activity/', include('actstream.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^comments/', include('django_comments_xtd.urls')),
    url(r'^', include('filer.server.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
