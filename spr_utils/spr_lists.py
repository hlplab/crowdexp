import unicodedata
import os.path
import re
from copy import deepcopy
from itertools import chain
from operator import itemgetter
from os import makedirs
from random import randint, shuffle
from typing import Dict, List, Tuple

import pandas as pd

try:  # stupid hack to make it work as a module or script
    from .tagged_spr_parser import Region, parse_tagged_sentence
except (ModuleNotFoundError, ImportError):
    from tagged_spr_parser import Region, parse_tagged_sentence

__all__ = ('write_spr_files',)


def slugify(value: str) -> str:
    """
    Simplified version of Django's slugify w/o Django dependency.

    Removed the code path for allowing unicode names

    Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


def region_to_dict(region: Region) -> dict:
    """Convert Region namedtuple into a dict for ease of pickling."""
    return {'tag': region.tag, 'start': region.start, 'stop': region.stop}


def wrap_inc(num: int, max: int = 4) -> int:
    """Return next number in sequence [1..max], looping at max."""
    # TODO: ditch this in favor of using itertools.cycle(range(1, max+1))
    if num == max:
        return 1
    else:
        if num > max:
            return num % max + 1
        else:
            return num + 1


def generate_config_file(listname: str) -> str:
    """Output the generic Linger-esque config file for a given list."""
    return f"""set	randomize	false\
\nset	insertBreaks	true\
\ninstruct	Thank you for taking part in our study!\
\ninstruct	In this task you'll read simple sentences and answer yes/no questions about them.<br/>Instead of seeing a whole sentence straight away, you will just see a word at a time. You can read the sentence at your own pace by pressing the space bar when you are ready to see the next word.\
\ninstruct	To start with, all you will see is a horizontal line. This line has a sentence "hidden" behind it.<br/><br/>To read the sentence, you will have to press the space bar. This will make the line disappear piece by piece, so that you can see the hidden words.<br/><br/>Every time a new word is revealed, the previous word will be hidden again. There is no way to return to words you have already read once they are hidden, so please read carefully.\
\ninstruct	Please read silently, not aloud. Once you have read each sentence, we will ask you question about it.<br/><br/>When you're ready to try some practice sentences, click "OK".	So far so good? Y\
\npractice	This sentence is a practice sentence to help you get used to a moving window display.	So far so good?	Y\
\npractice	This sentence is also for practice, and is followed by a question.	Are you following this so far?	Y\
\npractice	Now you will get to see what happens if you answer a question incorrectly.	Press the "yes" button now, ok?	N\
\ninstruct	OK, that's the end of the instructions! You'll get used to it quickly.<br/><br/>When you click OK, you can start reading the first "real" sentence. Please read carefully, and do your best to answer the questions correctly!<br/><br/>We will not be able to pay for HITs that are submitted incomplete or with more than a quarter of questions answered incorrectly.<br/><br/>Good luck!\
\nitems	{listname}.itm\
\nend	Thanks for participating. Please take a moment to give us feedback on this experiment in the box below. Then click on the "Submit" button below to complete the HIT and return to Mechanical Turk. Goodbye!"""


def flatten_condition(condname: str) -> str:
    """Lowercase condition names and replace spaces with underscores."""
    return condname.lower().replace(' ', '_')


def create_lists(excel_file: str) -> Tuple[List[Dict[str, str]], pd.DataFrame]:
    """Create Linger-esque item files from lists in an Excel file."""

    df = pd.read_excel(excel_file)

    df['Condition'] = df['Condition'].apply(flatten_condition)

    # get all unique conditions except the '-' filler condition
    conditions = set(df['Condition'].unique().tolist()) - set('-')
    numbers_to_conds = dict(zip(range(1, len(conditions) + 1), conditions))
    conds_to_numbers = dict(zip(conditions, range(1, len(conditions) + 1)))

    # get fillers.
    fillers = df[['ItemName', 'Experiment', 'Sentence', 'Condition', 'Question', 'Answer']].where(df['Experiment'] == 'filler').dropna()
    fillers['ItemName'] = fillers.ItemName.astype(int)

    # get critical items and their numbers
    expt_items = df[['ItemName', 'Experiment', 'Sentence', 'Condition', 'Question', 'Answer']].where(df['Experiment'] != 'filler').dropna()
    expt_items['ItemName'] = expt_items.ItemName.astype(int)

    # If there were a destructuring way of doing  this in one whack, I'd prefer it
    parsed_expt_sentences = expt_items['Sentence'].apply(parse_tagged_sentence)
    expt_items['Regions'] = parsed_expt_sentences.apply(lambda x: x.tags)
    expt_items['WordList'] = parsed_expt_sentences.apply(lambda x: x.wordlist)
    expt_items['Parsed'] = parsed_expt_sentences.apply(lambda x: x.parsed)

    # If there were a destructuring way of doing  this in one whack, I'd prefer it
    parsed_filler_sentences = fillers['Sentence'].apply(parse_tagged_sentence)
    fillers['Regions'] = parsed_filler_sentences.apply(lambda x: x.tags)
    fillers['WordList'] = parsed_filler_sentences.apply(lambda x: x.wordlist)
    fillers['Parsed'] = parsed_filler_sentences.apply(lambda x: x.parsed)

    regions = pd.concat([expt_items.loc[:, ['ItemName', 'Condition', 'Regions']], fillers.loc[:, ['ItemName', 'Condition', 'Regions']]])
    regions['Regions'] = regions['Regions'].apply(lambda row: [region_to_dict(x) for x in row])

    # Now we want the fillers as a list of dicts
    filler_dicts = fillers.to_dict(orient='records')

    # gotta remember what this is about
    expt_len = int(len(expt_items) / len(conditions))
    square_indices = {numbers_to_conds[i + 1]: range(1, expt_len + 1)[i::len(conditions)] for i in range(len(conditions))}

    # Make a dictionary keyed on condition with item lists
    cond_lists = {cond: expt_items.loc[expt_items['Condition'] == cond].to_dict(orient='records') for cond in conditions}

    # Generate one list of experimental items
    list_square = []
    for c in conditions:
        list_square.extend([x for x in cond_lists[c] if x['ItemName'] in square_indices[c]])

    # XXX: this is equivalent, yes? HAHAHA. No. Doesn't flatten.
    # list_square = [[x for x in cond_lists[c] if x['ItemName'] in square_indices[c]] for c in conditions]

    # Shuffle the items before we generate the final list
    shuffle(list_square)
    shuffle(filler_dicts)

    # Start filler insertion by putting them every other item if at least as many fillers as critical
    if len(filler_dicts) >= expt_len:
        list_square = list(chain.from_iterable(zip(list_square, filler_dicts[:expt_len])))
    else:  # Append any fillers and shuffle
        list_square.extend(filler_dicts)
        shuffle(list_square)

    if len(filler_dicts) > expt_len:
        # Insert a filler at the beginning and end of the list
        list_square.insert(0, filler_dicts[expt_len])

        # Then randomly insert fillers until we're out
        for f in filler_dicts[expt_len + 1:]:
            while 1:
                insert_idx = randint(0, len(list_square) - 1)
                # take a look at a slice 2 back and forward and make sure
                # there aren't 3 or more fillers
                if [x['Experiment'] for x in list_square[insert_idx - 1:insert_idx + 3]].count('filler') < 3:
                    list_square.insert(insert_idx, f)
                    break

    # Find the indices of each of the experimental items for each condition
    item_conds = [(x['ItemName'], x['Condition']) for x in list_square]
    cond_indices = {c: [dict(zip(('Index', 'ItemName'), (i, x[0]))) for i, x in enumerate(item_conds) if x[1] == c] for c in conditions}

    # Start off our final lists by making the first one a copy of the base list
    final_lists = [deepcopy(list_square)]

    oldlist = list_square
    for j in range(len(numbers_to_conds) - 1):
        newlist = deepcopy(oldlist)
        for k, v in cond_indices.items():
            new_cond = numbers_to_conds[wrap_inc(conds_to_numbers[k] + j, len(conditions))]
            items = sorted(v, key=itemgetter('Index'))

            replacements = [x for x in cond_lists[new_cond] if x['ItemName'] in [y['ItemName'] for y in items]]
            for i in items:
                idx, itm_name = i['Index'], i['ItemName']
                # Gets the matching item or None (should always match)
                # XXX: this is really inefficient; searches whole list every time
                newlist[idx] = next((x for x in replacements if x['ItemName'] == itm_name), None)
        final_lists.append(newlist)
        oldlist = newlist

    # create reversed lists by mapping reverse over existing and then appending result?
    rev_lists = [list(x) for x in map(reversed, final_lists)]

    # And then add the reversed lists to the final lists
    final_lists.extend(rev_lists)

    # Make a list of dicts with listname and contents
    outdata = [{'sheet': f'list{i}', 'contents': stimlist} for i, stimlist in enumerate(final_lists, 1)]

    return outdata, regions


def write_spr_files(experiment_name: str, excel_file: str, list_dir: str, base_path: str, analysis_dir: str) -> List[str]:
    """
    Write Linger-style SPR stimuli files to disk.

    Given a list of dicts with sheetnames as keys, where each dict has as its
    value a list of dicts with values of each row, output a directory of
    Linger-style stimuli files, one per dict
    """
    # create directory with dirname
    fullpath = os.path.join(base_path, list_dir)
    if not os.path.exists(fullpath):
        makedirs(fullpath)

    sprdata, regions = create_lists(excel_file=excel_file)

    list_names = []
    for row in sprdata:
        sheet = row['sheet']
        contents = row['contents']

        list_names.append(sheet)
        sprdir = os.path.join(fullpath, sheet)
        if not os.path.exists(sprdir):
            makedirs(sprdir)

        with open(os.path.join(sprdir, f'{sheet}.itm'), 'w') as sprfile:
            for item in contents:
                item_df = pd.DataFrame(item)
                sent_cols = sorted(item_df.filter(regex="Parsed\d*").columns.tolist())
                quest_cols = sorted(item_df.filter(regex="Question\d*").columns.tolist())
                ans_cols = sorted(item_df.filter(regex="Answer\d*").columns.tolist())
                for _, r in item_df.iterrows():
                    print(f"# {r['Experiment']} {r['ItemName']} {r['Condition']}", file=sprfile)
                    for s in sent_cols:
                        print(r[s], file=sprfile)
                    for q, a in zip(quest_cols, ans_cols):
                        print(f'? {r[q]} {r[a]}', file=sprfile)

        with open(os.path.join(sprdir, 'config'), 'w') as configfile:
            print(generate_config_file(sheet), file=configfile)

    if not os.path.exists(analysis_dir):
        makedirs(analysis_dir)
    regions.to_pickle(os.path.join(analysis_dir, f'{slugify(experiment_name)}.regions.pickle'))

    return list_names


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Write SPR trial lists')
    parser.add_argument('-e', '--experiment_name', required=True, help='Name of the experiment')
    parser.add_argument('-x', '--excel_file', required=True, help='Excel file to generate lists from')
    parser.add_argument('-l', '--region_file', required=False, default=None, help='Region mapping pickle file')
    parser.add_argument('-b', '--results_base_dir', required=True, help='Name of the results directory to write to')
    parser.add_argument('-a', '--analysis_dir', required=True, help='Directory where analysis will be performed.\n'
                                                                    'Pickle file with region mapping will be placed here')
    args = parser.parse_args()

    write_spr_files(experiment=args.experiment_name,
                    excel_file=args.excel_file,
                    list_dir=args.list_dir,
                    base_path=args.base_path,
                    analysis_dir=args.analysis_dir)


