import re
from collections import Sequence
from datetime import datetime
from os import makedirs, path
from typing import List, Optional, Union
from uuid import uuid4

from numpy import mean

import pandas as pd


def pipesplit(c: str) -> Union[List[str], str]:
    """Split a column on the pipe character."""
    try:
        return [x.strip() for x in c.split('|') if x.strip() != '']
    except:
        return c


def convert_results(experiment: str, results_base_folder: str, resultsfile: str, regionfile: Optional[str] = None) -> str:
    """
    Load results tab delimited file from MTurk and output a tab delim file in analyzable format.

    :param experiment: The name of experiment whose results will be converted
    :param results_base_folder: Path to where results file will be placed
    :param resultsfile: Unconverted results file to read in
    :param regionfile: Region info file to read in
    :return: string of path to converted results file
    """
    timestamp = datetime.now().isoformat()
    df = pd.read_csv(resultsfile, sep='\t')

    regions = pd.read_pickle(regionfile) if regionfile else pd.DataFrame()

    results_folder = path.join(results_base_folder, experiment)
    if not path.exists(results_folder):
        makedirs(results_folder)

    workers = df['workerid'].unique().tolist()

    newids = {w: uuid4().hex for w in workers}
    df['UUID'] = df.apply(lambda r: newids[r['workerid']], axis=1)
    # TODO: set path for file based on outfolder
    df[['workerid', 'UUID']].to_csv(path.join(results_folder, f'worker_map_{timestamp}.csv'), sep='\t', index=False)

    tre = re.compile(r'Answer\.trial([1-9]\d*)')
    wre = re.compile(r'Answer\.words([1-9]\d*)')
    rre = re.compile(r'Answer\.rts([1-9]\d*)')

    all_columns = df.columns.tolist()
    trial_columns = {int(tre.match(c).groups()[0]): c for c in all_columns if tre.match(c)}
    word_columns = {int(wre.match(c).groups()[0]): c for c in all_columns if wre.match(c)}
    rts_columns = {int(rre.match(c).groups()[0]): c for c in all_columns if rre.match(c)}
    spr_columns = list(trial_columns.values()) + list(word_columns.values()) + list(rts_columns.values())

    for col in spr_columns:
        df[col] = df[col].apply(pipesplit)

    # Just select the columns we care about for output
    df = df[spr_columns + ['UUID', ]]

    # Memoize  key off expname, cond, and item as tuple, with words and regions as value
    # e.g. {('filler', '-', 1): { words: [...], region_list: [...]}}
    memo = {}
    trialdfs = []
    for idx, row in df.iterrows():
        for trial_num in trial_columns.keys():
            expname, cond, item, resp = row[trial_columns[trial_num]]
            raw_resp = list(resp)
            correct = ', '.join([{'T': 'Correct', 'F': 'Incorrect'}[x] for x in raw_resp])
            qscore = mean([{'T': 100, 'F': 0}[x] for x in raw_resp])
            curr_rts = row[rts_columns[trial_num]]

            memo_key = (expname, cond, item)
            if memo_key in memo:
                curr_words = memo[memo_key]['words']
                region_list = memo[memo_key]['region_list']
            else:
                curr_words = row[word_columns[trial_num]]
                region_list = ['-' for _ in range(len(curr_words))]  # start with no region for anything
                if not regions.empty:
                    # This gets the row (should be exactly one) with a given item name and condition
                    trial_idx = regions.loc[(regions['ItemName'] == int(item)) & (regions['Condition'] == cond)].index[0]
                    trial_regions = regions.get_value(trial_idx, 'Regions')
                    for t in trial_regions:
                        for i in range(t['start'], t['stop']):
                            region_list[i] = t['tag']
                memo[memo_key] = {
                    'words': curr_words,
                    'region_list': region_list
                }

            word_range = range(len(curr_words))
            indices = [x for x in word_range]

            if not isinstance(curr_rts, Sequence):
                curr_rts = [0 for _ in range(len(curr_words) + 1)]


            # TODO: validate length of words vs rts and log if incorrect.
            # remember 1st RT is time until press to see 1st word
            tdf = pd.DataFrame({
                'Exp': pd.Series([expname for _ in word_range], index=indices),
                'Cond': pd.Series([cond for _ in word_range], index=indices),
                'Item': pd.Series([item for _ in word_range], index=indices),
                'Subj': pd.Series([row['UUID'] for _ in word_range], index=indices),
                'Trial': pd.Series([trial_num for _ in word_range], index=indices),
                'WordPos': pd.Series(indices, index=indices),
                'Word': pd.Series([w for w in curr_words], index=indices),
                'RegionName': pd.Series(region_list, index=indices),
                'RawRT': pd.Series([r for r in curr_rts[1:len(curr_words) + 1]], index=indices),
                'RawQuestion': pd.Series([correct for _ in word_range], index=indices),
                'Question': pd.Series([qscore for _ in word_range], index=indices),
            })
            trialdfs.append(tdf)
    out_df = pd.concat(trialdfs, ignore_index=True)
    outfile_path = path.join(results_folder, f'results_converted_{timestamp}.csv')
    out_df.to_csv(outfile_path, sep='\t', index=False)
    return outfile_path


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Convert Amazon Mechanical Turk results file for SPR experiment')
    parser.add_argument('-e', '--experiment_name', required=True, help='Name of the experiment being converted')
    parser.add_argument('-f', '--results_file', required=True, help='Filename to convert')
    parser.add_argument('-r', '--region_file', required=False, default=None, help='Region mapping pickle file')
    parser.add_argument('-b', '--results_base_dir', required=True, help='Name of the results directory to write to')
    args = parser.parse_args()

    convert_results(experiment=args.experiment_name,
                    results_base_folder=args.results_base_dir,
                    resultsfile=args.results_file,
                    regionfile=args.region_file)

# time python convert_results.py -e GenderedRelationShipBiases -f ~/venvs/bcs152SPR/SPRRunner/mturk/GenderedRelationshipBiases.final.results.csv -r ~/IdeaProjects/CrowdExp/analysis/gendered-relationship-biases-and-anaphoric-processing-experiment-1.regions.pickle -b ~/venvs/bcs152SPR/SPRRunner/mturk/results/

# pre memo
# real	0m8.462s
# user	0m8.355s
# sys	0m0.163s

# post memo
# real	0m7.042s
# user	0m6.887s
# sys	0m0.157s
