boto3==1.7.62  # 1.4.3 is 1st version w/ mturk support
botocore==1.10.62
mysqlclient==1.3.13
numpy==1.15.0
pandas==0.23.3
pyparsing==2.2.0
python-dateutil==2.7.3
openpyxl==2.5.4
xlrd==1.1.0
xlwt==1.3.0
psycopg2==2.7.5
