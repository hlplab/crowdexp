from celery import shared_task

from celery.utils.log import get_task_logger

from experiment_runner.utils.notifications import process_notifications

logger = get_task_logger(__name__)


@shared_task
def pump_queue():
    logger.info('Fetching notifications from SQS queue')
    process_notifications()
