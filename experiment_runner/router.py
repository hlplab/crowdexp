# based on https://newcircle.com/s/post/1242/django_multiple_database_support
class SPRRunnerRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on experiment_runner models to 'sprrunner'"
        if model._meta.app_label == 'experiment_runner':
            return 'sprrunner'
        return 'default'

    def db_for_write(self, model, **hints):
        "Point all operations on experiment_runner models to 'sprrunner'"
        if model._meta.app_label == 'experiment_runner':
            return 'sprrunner'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in experiment_runner app"
        if obj1._meta.app_label == 'experiment_runner' and obj2._meta.app_label == 'experiment_runner':
            return True
        # Allow if neither is experiment_runner app
        elif 'experiment_runner' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_syncdb(self, db, model):
        if db == 'sprrunner' or model._meta.app_label == "experiment_runner":
            return False  # we're not using syncdb on our legacy database
        else:  # but all other models/databases are fine
            return True
