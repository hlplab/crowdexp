import json
import logging

import boto3

from dateutil.parser import parse

from django.conf import settings

from ..models import NotificationEvent, HITId, HITTypeId, Assignment

from .mturk import fetch_process_notify_hit

logger = logging.getLogger(__name__)

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

SQS_REGION = AWS_SETTINGS['SQS_REGION']
SQS_QUEUE = AWS_SETTINGS['SQS_QUEUE']
AWS_ACCESS_KEY = AWS_SETTINGS['AWS_ACCESS_KEY']
AWS_SECRET_KEY = AWS_SETTINGS['AWS_SECRET_KEY']

assignment_map = {
    'AssignmentAccepted': 'InProcess',
    'AssignmentAbandoned': 'Abandoned',
    'AssignmentReturned': 'Returned',
    'AssignmentSubmitted': 'Submitted'
}

hit_status_map = {
    'HITCreated': 'Created',
    'HITExtended': 'Extended',
    'HITDisposed': 'Disposed',
    'HITReviewable': 'Reviewable',
    'HITExpired': 'Expired'
}


def process_notifications() -> None:
    """Fetch and process all notifications from SQS queue.

    Fetch all notification from the SQS queue
    Create NotificationEvents for each
    Update the state of any affected Assignments.
    """
    sqs_resource = boto3.resource('sqs')

    queue = sqs_resource.get_queue_by_name(QueueName=SQS_QUEUE)

    logger.info(f"Fetching {queue.attributes.get('ApproximateNumberOfMessages', 0)} messages from SQS")

    while int(queue.attributes.get('ApproximateNumberOfMessages', 0)) > 0:
        for message in queue.receive_messages(AttributeNames=['All'], MaxNumberOfMessages=10):
            msg = json.loads(message.body)
            docid = msg['EventDocId']
            for e in msg['Events']:
                timestamp = parse(e['EventTimestamp'])
                hittypeid, _ = HITTypeId.objects.get_or_create(hittypeid=e['HITTypeId'])
                hitid, hit_created = HITId.objects.get_or_create(hitid=e['HITId'], hittypeid=hittypeid)
                note_event, _ = NotificationEvent.objects.get_or_create(event_type=e['EventType'],
                                                                        eventdocid=docid,
                                                                        hittypeid=hittypeid,
                                                                        hitid=hitid,
                                                                        event_time=timestamp
                                                                        )
                assignment = None
                if e['EventType'] in ('AssignmentAccepted', 'AssignmentAbandoned',
                                      'AssignmentReturned', 'AssignmentSubmitted'):
                    assignment, assignment_created = Assignment.objects.get_or_create(assignmentid=e['AssignmentId'])
                    if assignment.state != assignment_map[e['EventType']]:
                        # notifications can come out of order; don't allow update
                        # to 'Accepted' if already 'Returned', 'Abandoned', or 'Submitted'
                        if assignment_created or not assignment.state_time or timestamp > assignment.state_time:
                            assignment.state = assignment_map[e['EventType']]
                            assignment.state_time = timestamp
                            assignment.save()

                if e['EventType'] in ('HITCreated', 'HITExtended', 'HITDisposed', 'HITReviewable', 'HITExpired') and (
                        hit_created or not hitid.status_time or timestamp > hitid.status_time):
                    # notifications can come out of order, don't backslide, e.g. from Reviewable to Created
                    hitid.hitstatus = hit_status_map[e['EventType']]
                    hitid.status_time = timestamp
                    hitid.save()

                    if e['EventType'] == 'HITReviewable':
                        fetch_process_notify_hit(hitid.hitid)
            # Only add the assignment to the NotificationEvent is new
                # and has no assignment connected to it
                if assignment and note_event.assignment is None:
                    note_event.assignment = assignment
                    note_event.save()

            message.delete()

# example SQS notification with event
# {u'CustomerId': u'A29979FPDJ98R8',
#  u'EventDocId': u'2d63dbfd28a75a32325390ed2acd193dbe7cdfcd',
#  u'EventDocVersion': u'2006-05-05',
#  u'Events': [{u'AssignmentId': u'1234567890123456789012345678901234567890',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'},
#              {u'AssignmentId': u'1234567890123456789012345678900987654321',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'}]}
