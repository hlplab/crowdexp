"""Utilities for processing Mechanical Turk results."""

from datetime import datetime
from os import makedirs, path
from typing import Dict, List, Set, Tuple

from actstream import action

import boto3

from django.conf import settings
from django.utils.text import slugify

from experiment_creator.models import MTurkHIT
from experiment_creator.util.contact import email_course_group, email_mturk_runners

from spr_utils.convert_results import convert_results

import pandas as pd

import xmltodict

from ..models import HITTypeId

ANALYSIS_DIRECTORY = getattr(settings, 'CROWDEXP', None).get('ANALYSIS_DIRECTORY', None)


def manage_url(hitid: str, sandbox: bool=False) -> str:
    """URL for managing a given HITId."""
    mturk_website = 'requestersandbox.mturk.com' if sandbox else 'requester.mturk.com'
    return f'https://{mturk_website}/mturk/manageHIT?HITId={hitid}'


def process_assignment(assignment: dict, hitinfo: dict, sandbox: bool=False) -> Tuple[Dict[str, str], Set]:
    """Turn an Assignment dict as returned by boto3 into a row for results file."""
    optional_assignment_keys = {
        'AcceptTime': 'assignmentaccepttime', 'RejectTime': 'assignmentrejecttime', 'Deadline': 'deadline',
        'RequesterFeedback': 'feedback', 'ApprovalTime': 'assignmentapprovaltime'
    }
    row: Dict[str, str] = {
        'assignmentid': assignment['AssignmentId'],
        'assignmentstatus': assignment['AssignmentStatus'],
        'autoapprovaltime': assignment['AutoApprovalTime'],
        'hitid': assignment['HITId'],
        'viewhit': manage_url(assignment['HITId'], sandbox),
        'assignmentsubmittime': assignment['SubmitTime'],
        'workerid': assignment['WorkerId'],
        # these assignment keys are optional
        'assignmentaccepttime': '',
        'assignmentapprovaltime': '',
        'assignmentrejecttime': '',
        'deadline': '',
        'feedback': '',
        # 'reject' is for processing results files to mark which rows are to be rejected with an x
        'reject': '',
        # HIT keys.
        'hittypeid': hitinfo['HITTypeId'],
        'hitgroupid': hitinfo['HITGroupId'],
        'title': hitinfo['Title'],
        'description': hitinfo['Description'],
        'keywords': hitinfo['Keywords'],
        'reward': '$' + hitinfo['Reward'],
        'creationtime': hitinfo['CreationTime'],
        'assignments': hitinfo['MaxAssignments'],
        'numavailable': hitinfo['NumberOfAssignmentsAvailable'],
        'numpending': hitinfo['NumberOfAssignmentsPending'],
        'numcomplete': hitinfo['NumberOfAssignmentsCompleted'],
        'hitstatus': hitinfo['HITStatus'],
        'reviewstatus': hitinfo['HITReviewStatus'],
        'assignmentduration': hitinfo['AssignmentDurationInSeconds'],
        'autoapprovaldelay': hitinfo['AutoApprovalDelayInSeconds'],
        'hitlifetime': hitinfo['Expiration'],
        'annotation': ''
    }

    # populate the optional keys if they exist
    for k, v in optional_assignment_keys.items():
        if k in assignment:
            row[v] = assignment[k]

    assignment_keys = set()
    if 'QualificationRequirements' in hitinfo:
        for i, qual in enumerate(['|'.join([f'{k}:{v}' for k, v in x.items()]) for x in hitinfo['QualificationRequirements']]):
            qualkey = f'Qualification.{i}'
            row[qualkey] = qual
            assignment_keys.add(qualkey)

    if 'RequesterAnnotation' in hitinfo:
        row['annotation'] = hitinfo['RequesterAnnotation']

    # answers are in assignment['Answer'] as an MTurk QuestionFormAnswers XML string
    ordered_answers = xmltodict.parse(assignment.get('Answer')).get('QuestionFormAnswers').get('Answer')
    # FIXME: answer might not be FreeText, but that's what I'm handling for now
    # http://docs.aws.amazon.com/AWSMechTurk/latest/AWSMturkAPI/ApiReference_QuestionFormAnswersDataStructureArticle.html
    # http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd
    user_answers = {f'Answer.{d["QuestionIdentifier"]}': d['FreeText'] for d in ordered_answers}
    assignment_keys.update(set(user_answers.keys()))
    row.update(user_answers)

    return row, assignment_keys


def get_results(hitids: List[str], sandbox: bool=False) -> pd.DataFrame:
    """Give list of HITIds, return DataFrame of results."""
    endpoint = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com' if sandbox else 'https://mturk-requester.us-east-1.amazonaws.com'
    mtc = boto3.client('mturk', endpoint_url=endpoint, region_name='us-east-1')  # Currently only us-east-1 has MTurk endpoint

    all_results = []
    outkeys = ['hitid', 'hittypeid', 'hitgroupid', 'title', 'description', 'keywords', 'reward',
               'creationtime', 'assignments', 'numavailable', 'numpending', 'numcomplete',
               'hitstatus', 'reviewstatus', 'annotation', 'assignmentduration',
               'autoapprovaldelay', 'hitlifetime', 'viewhit', 'assignmentid', 'workerid',
               'assignmentstatus', 'autoapprovaltime', 'assignmentaccepttime',
               'assignmentsubmittime', 'assignmentapprovaltime', 'assignmentrejecttime',
               'deadline', 'feedback', 'reject']
    answer_keys = set()

    hits = {h: mtc.get_hit(HITId=h).get('HIT') for h in hitids}
    for h in hitids:
        response = mtc.list_assignments_for_hit(HITId=h)
        assignments = response.get('Assignments')
        while response.get('NumResults', 0) >= 10:  # XXX: does this work or do I need to do != 0
            # I assume 10 is the biggest number they show, but it'd be nice if it decreased with each page
            response = mtc.list_assignments_for_hit(HITId=h, NextToken=response.get('NextToken'))
            assignments.extend(response.get('Assignments'))
        for assignment in assignments:
            row, assignment_keys = process_assignment(assignment, hits[h], sandbox)
            all_results.append(row)
            answer_keys.update(assignment_keys)
    outkeys.extend(list(sorted(answer_keys)))

    return pd.DataFrame(all_results, columns=outkeys)

def fetch_process_notify_hit(hitid: str) -> None:
    """Fetch results for a given HITId, process them, and notify relevant people."""
    mturk_hit = MTurkHIT.objects.filter(props__HITId=hitid).first()
    hit_group = [h.props.get('HITId', None) for h in MTurkHIT.objects.filter(experiment=mturk_hit.experiment).all() if h.props.get('HITStatus', None) != 'Disposed']
    results = get_results(hitids=hit_group, sandbox=mturk_hit.sandbox)
    outpath = path.join(ANALYSIS_DIRECTORY, slugify(mturk_hit.experiment.name))
    result_time = datetime.now().isoformat()
    if not path.exists(outpath):
        makedirs(outpath)
    results_filename = path.join(outpath, f'results.{result_time}.csv')
    results.to_csv(results_filename, index=False, sep='\t')
    action.send(mturk_hit.experiment, verb='results were downloaded')
    subject_download = f'Results downloaded for {mturk_hit.experiment.name}'
    message_download = f'Results available at {results_filename}'
    email_course_group(mturk_hit.experiment.group_id,
                       subject_download,
                       message_download)
    email_mturk_runners(subject_download, message_download)
    converted_result_file = convert_results(experiment=slugify(mturk_hit.experiment.name),
                                            results_base_folder=ANALYSIS_DIRECTORY,
                                            resultsfile=results_filename,
                                            regionfile=path.join(ANALYSIS_DIRECTORY,
                                                                 f'{slugify(mturk_hit.experiment.name)}.regions.pickle'))
    action.send(mturk_hit.experiment, verb='results were processed')
    subject_processed = f'Results processed for {mturk_hit.experiment.name}'
    message_processed = f'Converted results available at {converted_result_file}'
    email_course_group(mturk_hit.experiment.group_id,
                       subject_processed,
                       message_processed)
    email_mturk_runners(subject_processed, message_processed)
