from django.contrib import admin
from .models import Worker, Experiment, TrialList, Assignment, HITId, HITTypeId, NotificationEvent


class WorkerAdmin(admin.ModelAdmin):
    list_display = ('workerid',)


class ExperimentAdmin(admin.ModelAdmin):
    list_display = ('name',)


class TrialListAdmin(admin.ModelAdmin):
    list_display = ('name', 'number', 'experiment')


class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('assignmentid', 'state', 'list', 'worker', 'experiment')


class HITIdAdmin(admin.ModelAdmin):
    list_display = ('hitid', 'hittypeid')


class HITTypeIdAdmin(admin.ModelAdmin):
    list_display = ('hittypeid',)


class NotificationEventAdmin(admin.ModelAdmin):
    list_display = ('event_type', 'event_time', 'eventdocid', '_assignment', '_hitid', '_hittypeid')

    def _assignment(self, obj):
        try:
            return obj.assignment.assignmentid
        except AttributeError:
            return '---'

    _assignment.empty_value_display = '---'

    def _hitid(self, obj):
        try:
            return obj.hitid.hitid
        except AttributeError:
            return '---'

    _hitid.empty_value_display = '---'

    def _hittypeid(self, obj):
        try:
            return obj.hittypeid.hittypeid
        except AttributeError:
            return '---'

    _hittypeid.empty_value_display = '---'


admin.site.register(Worker, WorkerAdmin)
admin.site.register(Experiment, ExperimentAdmin)
admin.site.register(TrialList, TrialListAdmin)
admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(HITId, HITIdAdmin)
admin.site.register(HITTypeId, HITTypeIdAdmin)
admin.site.register(NotificationEvent, NotificationEventAdmin)
