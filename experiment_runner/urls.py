from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^experiments/$', views.experiments, name='experiment_list'),
    url(r'^experiments/(?P<experiment>[a-zA-Z0-9_-]+)/$', views.experiment_detail, name='experiment_detail'),
    url(r'^experiments/spr/demo/(?P<experiment>[a-zA-Z0-9_-]+)/$', views.spr_demo, name='spr_demo'),
    url(r'^experiments/spr/demo/(?P<experiment>[a-zA-Z0-9_-]+)/(?P<listid>[a-zA-Z0-9_]+)/$', views.spr_demo, name='spr_demo'),
    url(r'^experiments/spr/run/(?P<experiment>[a-zA-Z0-9_-]+)/$', views.spr_experiment, name='spr_experiment'),
    url(r'^echo/$', views.response_repeater, name='response_repeater'),
]
