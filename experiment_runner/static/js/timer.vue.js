Vue.component('experiment-timer', {
  data: function () {
    return {
      secondsElapsed: 0,
      minutes: 0,
      seconds: 0
    };
  },
  mounted: function() {
    this.$nextTick(function () {
      this.interval = setInterval(this.intervalCallback, 1000);
    });
  },
  beforeDestroy: function() {
    clearInterval(this.interval);
  },
  methods: {
    intervalCallback: function() {
      this.secondsElapsed++;
      this.minutes = Math.floor(this.secondsElapsed / 60);
      this.seconds = this.secondsElapsed % 60;
    }
  },
  template: `<div>Time Elapsed: {{this.minutes}} minutes, {{this.seconds}} seconds</div>`,
});

Vue.component('duration-estimate', {
  data: function() {
    return {
      internal_estimate: 0,
      submit_disable: true
    };
  },
  model: {
    prop: 'minute_estimate',
    event: 'change'
  },
  props: {
    minute_estimate: {
      type: Number,
      default: 0,
      required: true
    }
  },
  watch: {
    'internal_estimate': function() {
      // When the internal value changes, we $emit an event. Because this event is
      // named 'change', v-model will automatically update the parent value
      this.$emit('change', parseInt(this.internal_estimate));
    },
    'minute_estimate': function() {
      // because we're binding the model to 'change' instead of 'input', 'value' doesn't get updated unless we do this
      this.internal_estimate = this.minute_estimate;
    }
  },
  created: function() {
    // We initially sync the internal value with the value passed in by the parent
    this.internal_estimate = this.minute_estimate;
  },
  methods: {
    getTimeEstimate: function() {
      $.ajax({
        url: vm.estimate_url,
        data: {
          experiment: uploadInfo.experiment
        },
        method: 'GET'
      }).done(function(data, textStatus, jqXHR) {
        console.log(data);
        vm.minute_estimate = uploadInfo.admin ? data.instructor_duration : data.group_duration;
      }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR.status + ' ' + jqXHR.statusText + ': ' + jqXHR.responseText);
      }).always(_.noop());
    },
    setTimeEstimate: function() {
      $.ajax({
        url: vm.estimate_url,
        data: {
          time_estimate: this.minute_estimate,
          experiment: uploadInfo.experiment,
          is_staff: uploadInfo.admin
        },
        method: 'POST',
        beforeSend: (xhr, settings) => {
          xhr.setRequestHeader('X-CSRFToken', Cookies.get('csrftoken'));
        },
      }).done(function(data, textStatus, jqXHR) {
        console.log('Data uploaded: ' + jqXHR.status + ': ' + jqXHR.responseText);
      }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR.status + ' ' + jqXHR.statusText + ': ' + jqXHR.responseText);
      }).always(_.noop());
    }
  },
  // mounted: function() {
  //   this.getTimeEstimate();
  // },
  template:
    `<div id="duration">
        <br/>
        <label for="time_estimate">Estimate (in minutes) of experiment duration</label>
        <input type="number" min="0" max="120" step="1" v-model.number="internal_estimate" id="time_estimate" name="time_estimate"/>
        <button id="submit_estimate" type="button" :disabled="this.submit_disabled">Submit estimate</button>
    </div>`
});

var vm = new Vue({
  el: '#container',
  data:  {
    minute_estimate: 25,
    estimate_url: uploadInfo.url
  }
});
