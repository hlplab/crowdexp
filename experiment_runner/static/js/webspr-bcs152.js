/* jshint browser: true, devel: true, jquery: true */
/* global swfobject, flashvars, swfurl, $ */

/* eslint-disable no-console, no-unused, no-unused-vars */

function register(field, answer) {
  'use strict';
  log('Registering: ' + field + ' with value: ' + answer);
  $('<input>').attr({
    type: 'hidden',
    id: field,
    name: field
  }).val(answer).appendTo('form');
  //document.getElementById(field).value = answer;
  return true;
}

function onExperimentEnd() {
  'use strict';
  userInfo.completed = true;
  $('#duration').css('background-color', 'rgba(255, 0, 0, 0.35)');
  $('#submit_estimate').removeAttr('disabled');
  $('#comment').show(function () {
    $('#commentarea').focus();
  });
  $('#submit').show(function () {
    $(this).removeAttr('disabled');
  });
}

function log(args) {
  'use strict';
  if (!_.isUndefined(console) && !_.isUndefined(console.log)) {
    console.log(args);
  }
}

function flashLoadedCB(e) {
  'use strict';
  if (!e.success) {
    var error_string = '';
    if (_.isEqual(swfobject.ua.pv, [0, 0, 0])) {
      error_string = '<p style="font-weight bold; color: red;">Flash plugin is not installed or is disabled</p>';
      document.getElementById('fixflash').style.display = 'block';
    } else if (swfobject.ua.pv[0] < 20) {
      error_string = '<p style="font-weight bold; color: red;">Flash plugin is too old: ' + swfobject.ua.pv.join('.') +
        ' is installed but 20.0.0 or newer is required. Please update your Flash player.</p>';
    } else {
      error_string = '<p style="font-weight bold; color: red;">Failed to load flash content.</p>';
    }
    document.getElementById('flashcontent').innerHTML = error_string;
  }
}

$(document).ready(function () {
  'use strict';
  $('input[name="browserid"]').val(navigator.userAgent);
  $('input[name="flashversion"]').val(swfobject.ua.pv.join('.'));
  var params = {allowScriptAccess: 'always', menu: 'false', bgcolor: '#ffffff', quality: 'high'};
  $('button#endinstr').on('click', function () {
    $('#instructions').hide(function () {
      swfobject.embedSWF(swfurl,
        'flashcontent', '950', '600', '20.0.0', null,
        flashvars, params, {}, function (e) {
          flashLoadedCB(e);
        });
    });
  });
});
