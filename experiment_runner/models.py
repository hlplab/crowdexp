from django.db import models
from django_mysql.models import EnumField


class Worker(models.Model):
    """
    Representation of an mturk worker and what list they were assigned to
    """
    workerid = models.CharField(max_length=32, unique=True, blank=True, null=True)

    def __repr__(self):
        return f'<Worker: "{self.workerid}">'

    class Meta:
        managed = False
        db_table = 'worker'

class Protocol(models.Model):
    """
    RSRB protocol.
    """
    name = models.CharField(max_length=128, unique=True)
    number = models.CharField(max_length=32, unique=True)

    def __repr__(self):
        return f'Protocol: {self.name} ({self.number})'

    class Meta:
        managed = False
        db_table = 'protocol'


class Experiment(models.Model):
    """
    Representation of an experiment
    """
    name = models.CharField(max_length=128, unique=True, blank=True, null=True)
    protocol = models.ForeignKey(Protocol, related_name='experiments', blank=True, null=True)
    # TODO: create a 'category' class (e.g. SPR, socalign) and make a foreign key relationship under Experiment.
    # general idea that we need a way to call the right view and templates for each experiment when generating demos

    def __repr__(self):
        return f'Experiment: "{self.name}"'

    class Meta:
        managed = False
        db_table = 'experiment'


class TrialList(models.Model):
    """
    Which list workers are on
    """

    name = models.CharField(max_length=32, blank=True, null=True)
    number = models.PositiveIntegerField(blank=True, null=True)
    experiment = models.ForeignKey(Experiment, related_name='lists', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return f'<TrialList: "{self.experiment}::{self.name}">'

    class Meta:
        managed = False
        unique_together = (('experiment', 'name'),)
        db_table = 'triallist'


class Assignment(models.Model):
    """
    A MTurk HIT assignment.
    """
    assignmentid = models.CharField(max_length=32, unique=True, blank=True, null=True)
    state = EnumField(choices=('InProcess', 'Submitted', 'Abandoned', 'Returned', 'Rejected', 'Approved'),
                      default='InProcess')
    state_time = models.DateTimeField(blank=True, null=True)

    list = models.ForeignKey(TrialList, related_name='assignments', on_delete=models.DO_NOTHING, null=True)
    worker = models.ForeignKey(Worker, related_name='assignments', on_delete=models.DO_NOTHING, null=True)
    experiment = models.ForeignKey(Experiment, related_name='assignments', on_delete=models.DO_NOTHING, null=True)

    def __repr__(self):
        try:
            return f'Assignment: "{self.assignmentid}" TrialList: {self.experiment.name}::{self.triallist.name}'
        except AttributeError:
            return f'Assignment: "{self.assignmentid}"'

    class Meta:
        managed = False
        db_table = 'assignment'


class HITTypeId(models.Model):
    """
    An Amazon Mechanical Turk HITTypeId
    """
    hittypeid = models.CharField(max_length=32, blank=True, null=True)

    def __repr__(self):
        return f'HITTypeId: "{self.hittypeid}"'

    @property
    def downloadable(self):
        return all([h.hitstatus in ('Reviewable', 'Expired') for h in self.HITS.all()])

    class Meta:
        managed = False
        db_table = 'hittypeid'


class HITId(models.Model):
    """
    An Amazon Mechanical Turk HITId
    """
    hitid = models.CharField(max_length=32, blank=True, null=True)
    hitstatus = EnumField(choices=('Created', 'Extended', 'Reviewable', 'Expired', 'Disposed'), default='Created')
    status_time = models.DateTimeField(blank=True, null=True)
    hittypeid = models.ForeignKey(HITTypeId, related_name='HITS', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return f'HITId: "{self.hitid}"'

    class Meta:
        managed = False
        db_table = 'hitid'


class NotificationEvent(models.Model):
    """
    An Amazon Mechanical Turk notification event.
    """
    event_type = EnumField(choices=('AssignmentAccepted', 'AssignmentAbandoned',
                                    'AssignmentReturned', 'AssignmentSubmitted',
                                    'HITReviewable', 'HITExpired', 'Ping'))
    event_time = models.DateTimeField(blank=True, null=True)
    eventdocid = models.CharField(max_length=48, blank=True, null=True)
    assignment = models.ForeignKey(Assignment, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)
    hitid = models.ForeignKey(HITId, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)
    hittypeid = models.ForeignKey(HITTypeId, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return f'Event: "{self.event_type}::{self.event_time}"'

    class Meta:
        managed = False
        db_table = 'notificationevent'
