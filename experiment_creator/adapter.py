from allauth.account.adapter import DefaultAccountAdapter
from allauth.exceptions import ImmediateHttpResponse

from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render


class AccountAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request):
        return reverse('user_profile', kwargs={'user_id': request.user.id})

    def is_open_for_signup(self, request):
        """
        Control if social account signups are possible.

        Return false to disable
        """
        return True

    def pre_social_login(self, request, sociallogin):
        """
        Before social account login, check that they are signing in from a valid domain.

        Overrides default method in allauth/socialaccount/adapter.py

        From http://stackoverflow.com/a/19232328/3846301
        """
        u = sociallogin.account.user
        # FIXME: also need to allow specific gmail accounts
        if not u.email.split('@')[1] in settings.ALLOWED_ACCOUNT_DOMAINS:
            raise ImmediateHttpResponse(render('experiment_creator/badsocial.html'))
