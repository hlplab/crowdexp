from dateutil import tz
from dateutil.parser import parse

from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name='user_icon', is_safe=True)
def user_icon(user, size=75):
    profile_url = user.profile.profile_image_url(size)
    return mark_safe(f'<div class="user-icon-{size}" style="background-image: url({profile_url});"></div>')


@register.filter(name='status_class')
def status_class(value):
    """Return a classname based on status passed."""
    try:
        return {
            'NEW': 'new',
            'UPL': 'needsaction',
            'REQ': 'needsaction',
            'APP': 'approved',
            'REV': 'revision',
            'RUP': 'needsaction',
        }[value]
    except KeyError:
        return ''


@register.filter(name='alert_level')
def alert_level(value):
    """Return Foundation classname for Django message level."""
    try:
        return {
            'debug': 'secondary',
            'info': 'primary',
            'success': 'success',
            'warning': 'warning',
            'error': 'alert'
        }[value]
    except KeyError:
        return ''


@register.filter(name='experiment_status')
def experiment_status(estatus, stage):
    """Return {warning,secondary,success} strings for class value on experiment stage."""
    stage = int(stage)
    try:
        status_level = {
            'DES': 1,
            'STM': 2,
            'GTS': 3,
            'STS': 4,
            'FAA': 5,
            'RUN': 6,
            'ANL': 7
        }[estatus]
        if status_level == 0 and stage == 1:
            return 'warning'
        if status_level == stage:
            return 'warning'
        elif status_level < stage:
            return 'secondary'
        else:
            return 'success'
    except KeyError:
        return ''


@register.filter(name='icon_level')
def icon_level(value):
    """Return Font Awesome icon for Django message level."""
    try:
        return {
            'debug': 'info-circle',
            'info': 'info-circle',
            'success': 'check-circle',
            'warning': 'exclamation-circle',
            'error': 'ban'
        }[value]
    except KeyError:
        return ''


@register.filter(name='file_icon')
def file_icon(value):
    """Return icon for file type."""
    try:
        return {
            'MS-Word': 'file-word-o',
            'MS-Excel': 'file-excel-o'
        }[value]
    except KeyError:
        return ''


@register.filter(name='regions_td', is_safe=True)
def regions_td(value):
    """
    Make <td> tags from a region tags.

    where value is a namedtuple('ParsedSentence', ['original', 'parsed', 'wordlist', 'tags'])
    """
    tds = ''
    base = 0
    width = len(value.wordlist)
    tags = value.tags
    if tags:
        tag_start = tags[0].start
        tag_end = tags[-1].stop

        if tag_start > 0:
            tds += f'<td colspan={tag_start}></td>'  # make an empty tag up to first tag
            base = tag_start

        for region in tags:
            if region.start > base:
                tds += f'<td colspan={region.start - base}></td>'  # make an empty tag to fill in
            tds += f'<td colspan={region.stop - region.start}>{region.tag}</td>'
            base = region.stop

        if tag_end < width:
            tds += f'<td colspan={width - tag_end}></td>'  # make an empty tag up to end of table
    else:
        tds = f'<td colspan={width}>No Regions</td>'

    return mark_safe(tds)


@register.filter(name='mturk_preview', is_safe=True)
def mturk_preview(groupid, sandbox=False):
    """Return the preview URL for an MTurk HIT."""
    preview_prefix = 'workersandbox' if sandbox else 'www'
    preview_tag = f'<a href="https://{preview_prefix}.mturk.com/mturk/preview?groupId={groupid}">Preview URL</a>'
    return mark_safe(preview_tag)


@register.filter(name='datestring_format')
def datestring_format(datestring):
    return parse(datestring).astimezone(tz=tz.tzlocal()).strftime('%A %m/%d/%y %I:%M:%S%p %Z')


@register.inclusion_tag('experiment_creator/render_proposal.html')
def render_proposal(design_proposal):
    return {'design': design_proposal}


@register.inclusion_tag('experiment_creator/render_stimuli.html')
def render_stimuli(experiment_stimuli):
    return {'stimuli': experiment_stimuli}


@register.inclusion_tag('experiment_creator/render_experiment_test.html', takes_context=True)
def render_experiment_test(context):
    return {'experiment': context['runner_experiment'],
            'user': context['user']}


@register.inclusion_tag('experiment_creator/render_user.html')
def render_user(user, icon_size=50):
    return {'user': user,
            'icon_size': icon_size}


@register.inclusion_tag('experiment_creator/projects_for_user.html')
def projects_for_user(user):
    return {'user': user}


@register.inclusion_tag('experiment_creator/groups_for_user.html')
def groups_for_user(user):
    return {'user': user}


@register.simple_tag
def vue_tag(tagContent):
    """Leave a mustache tag for Vue.js instead of the Django template rendering the variable."""
    return '{{%s}}' % tagContent
