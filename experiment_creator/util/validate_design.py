#!/usr/bin/env python3

"""Tests to validate user submitted stimuli designs for self paced reading."""

import itertools
import sys

from typing import Dict

from .testing import TestRegistration, TestResult


# """Check for common mistakes in StimuliDesign"""
# TODO: check that filler_count, participants, and exname all actually have values


def flatten(list_of_lists):
    """Flatten one level of nesting."""
    return list(itertools.chain.from_iterable(list_of_lists))


@TestRegistration.register
def at_least_one_critical_item(design: Dict) -> TestResult:
    """Test that there's at least one critical item."""
    test_name = 'There must be at least one critical item'
    try:
        assert int(design['trial_count']) > 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'message': '"Number of critical items" cannot be zero',
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def at_least_one_multilevel_manipulation(design: Dict) -> TestResult:
    """Test that there is at least one manipulation with two or more levels."""
    test_name = 'There must be at least one manipulation with two or more levels'
    manip_sizes = [len(x['items']) for x in design['manipulations']]
    try:
        assert len(manip_sizes) > 0 and max(manip_sizes, default=0) > 1
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'message': 'Your design does not have at least one manipulation with two or more levels',
            'problems': [{'Number of manipulations': len(design['manipulations']), 'Max levels': max(manip_sizes, default=0)}]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def more_fillers_than_items(design: Dict) -> TestResult:
    """Test there are more filler items than critical items."""
    test_name = 'There should be more filler items than critical items'
    # FIXME: false pass when both are zero
    try:
        assert int(design['trial_count']) < int(design['filler_count'])
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'message':
                'It seems like you have the same number or more items than fillers. '
                'This is not recommended, since it can make it more likely that participants '
                'will implicitly or explicitly infer the purpose of your '
                'experiment. Consider adding more fillers.',
            'problems': [{'Critical Item count': design['trial_count'], 'Filler Item count': design['filler_count']}]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def manipulations_exist(design: Dict) -> TestResult:
    """Test that `manipulations` is not empty."""
    test_name = 'Manipulations cannot be empty'
    try:
        assert design['manipulations']
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'message': 'Manipulations must exist. Any further manipulation related test results could be meaningless',
            'problems': [{'Manipulation names': 'Empty'}]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def manipulations_are_unique(design: Dict) -> TestResult:
    """Test that no two manipulations have the same name."""
    test_name = 'Manipulations must be unique'
    # FIXME: false pass when no manipulations
    manip_names = [x['name'] for x in design['manipulations']]
    try:
        assert len(manip_names) <= len(set(manip_names))
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'message': 'Manipulation names must be unique.',
            'problems': [{'Manipulation names': manip_names}]

        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def conditions_are_global_unique(design: Dict) -> TestResult:
    """Test that no two condition levels have the same name (even if it's across manipulations)."""
    test_name = 'Level names must be unique, even across manipulations'
    # FIXME: false pass when no manipulations
    level_names = flatten([x['items'] for x in design['manipulations']])
    try:
        assert len(level_names) <= len(set(level_names))
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'message': 'Level names should be unique even across manipulations',
            'problems': [{'Level names': level_names}]
        }
    else:
        return True, {'test': test_name}


# @TestRegistration.register
# def conditions_are_local_unique(design: Dict) -> TestResult:
#     """Test that no two condition levels have the same name within a manipulation"""
#     test_name = 'Level names must be unique within each manipulation'
#     for x in design['manipulations']:
#         if len(x['items']) > len(set(x['items'])):
#             errors['errors'].append({
#                 'category': 'conditions',
#                 'message': 'Condition names must be unique within a manipulation',
#                 'problem': x['items']
#             })


def validate_design(design: Dict) -> Dict:
    """Run all registered tests to validate stimuli design."""
    errors = []
    warnings = []
    passes = []

    for i, test in enumerate(TestRegistration.tests):
        try:
            doespass, result = test(design)
            if doespass:
                passes.append(result)
            else:
                if result['class'] == 'Warning':
                    warnings.append(result)
                else:  # 'Error' and 'Fatal'
                    errors.append(result)
        except Exception as e:
            errors.append({'test': test.__name__, 'category': 'Exception', 'message': e})
            print(f'Uh oh. `{test.__name__}` barfed: {e}', file=sys.stderr)

    return {'errors': errors, 'warnings': warnings, 'passes': passes, 'test_count': len(TestRegistration.tests),
            'pass_count': len(passes), 'error_count': len(errors), 'warning_count': len(warnings)}
