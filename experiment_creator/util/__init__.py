from hashlib import sha1
from os.path import basename, getsize, isdir
from typing import Dict, List


def get_files_info(files: List[str]) -> List[Dict[str, str]]:
    """Get file size and hash for a list of files."""
    return [
        {'basename': basename(f),
         'size': getsize(f),
         'hash': sha1(open(f, 'rb').read()).hexdigest(),
         } for f in files if not isdir(f)
    ]
