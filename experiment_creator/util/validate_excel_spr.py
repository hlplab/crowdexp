#!/usr/bin/env python3
import re
import sys
from itertools import product
from typing import Dict, List, Tuple

import pandas as pd

from spr_utils.tagged_spr_parser import test_tagged_sentence, parse_tagged_sentence

from .testing import TestRegistration, TestResult


@TestRegistration.register
def _test_headings(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test heading names exist and are correct."""
    headings = ['Experiment', 'ItemName', 'Condition', 'Sentence', 'Question', 'Answer']
    test_name = f'All and only column names must be: {", ".join(headings)}'

    # Check that the heading names are correct. Allow no slop.
    try:
        assert(df.columns.tolist() == headings)
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Fatal',
            'category': 'header',
            'message': 'Fatal Error. Header row should be "{}". You have: "{}".'
                       ' No further error checking can be done until this is corrected.'
                       .format('", "'.join(headings), '", "'.join(df.columns.tolist()))
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_missing_experiment_names(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that <ExperimentName> and filler are the passed in as experiment names."""
    test_name = 'All experiment names in design must be present'
    ex_names = df['Experiment'].unique().tolist()
    experiment = kwargs.get('experiment', 'NoExperimentNameProvided')
    fillercount = kwargs.get('fillercount', 9999999)
    valid_names = [experiment, 'filler'] if fillercount > 0 else [experiment]
    missing_names = set(valid_names) - set(ex_names)
    try:
        assert len(missing_names) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'row values',
            'message': f'The following experiments are in your design but not your stimuli: {missing_names}'
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_extra_experiment_names(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that no experiment names are passed in other than <ExperimentName> and filler."""
    test_name = 'No experiment names not in design are permitted'
    ex_names = df['Experiment'].unique().tolist()
    experiment = kwargs.get('experiment', 'NoExperimentNameProvided')
    valid_names = [experiment, 'filler']
    extra_names = set(ex_names) - set(valid_names)
    try:
        assert len(extra_names) == 0
    except AssertionError:
        bad_rows = df['Experiment'].where(df['Experiment'].isin(extra_names)).dropna()
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'row values',
            'message': f'The only acceptable values for "Experiment" are "{experiment}" and "filler".',
            'problems': [{'Experiment': n, 'Rows': [k.item() + 2 for k, v in bad_rows.items() if v == n]}
                         for n in extra_names]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_missing_condition_names(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that specified conditions and '-' exist."""
    test_name = 'All conditions in design must be present'
    cond_col = df['Condition'].unique().tolist()
    conditions = kwargs.get('conditions', (['bad', 'bad'], ['bad', 'bad']))
    fillercount = kwargs.get('fillercount', 9999999)
    valid_conds = [*['.'.join(x) for x in product(*conditions)]]
    if fillercount > 0:
        valid_conds.extend('-')
    missing_conds = set(valid_conds) - set(cond_col)
    try:
        assert len(missing_conds) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'conditions',
            'message': f'The following conditions are in your design but not your stimuli: {missing_conds}'
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_extra_condition_names(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that only conditions are the specified ones and '-'."""
    test_name = 'No conditions not in design are permitted'
    cond_col = df['Condition'].unique().tolist()
    conditions = kwargs.get('conditions', (['bad', 'bad'], ['bad', 'bad']))
    valid_conds = ['-', *['.'.join(x) for x in product(*conditions)]]
    extra_conds = set(cond_col) - set(valid_conds)
    try:
        assert len(extra_conds) == 0
    except AssertionError:
        bad_rows = df['Condition'].where(df['Condition'].isin(extra_conds)).dropna()
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'conditions',
            'message': 'Invalid condition name(s) found.',
            'problems': [{'Condition': n, 'Row': [k.item() + 2 for k, v in bad_rows.items() if v == n]} for n in extra_conds]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_item_row_count(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that each item has exactly as many rows as there are conditions."""
    test_name = 'Each item must have as many rows as there are conditions'
    conditions = kwargs.get('conditions', (['bad', 'bad'], ['bad', 'bad']))
    cond_count = len(['.'.join(x) for x in product(*conditions)])
    item_counts = df['ItemName'].where(df['Experiment'] != 'filler').value_counts()
    bad_counts = item_counts[~item_counts.eq(cond_count)]
    try:
        assert bad_counts.empty
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'conditions',
            'message': f'Each item should have {cond_count} conditions.',
            'problems': [{'Item': int(k), 'Count': v} for k, v in sorted(bad_counts.items())]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_filler_count(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that there are correct number of rows with the experiment 'filler'."""
    test_name = 'Number of filler rows must match number in design'
    fillercount = kwargs.get('fillercount', 9999999)
    try:
        numfillers = df['Experiment'].where(df['Experiment'] == 'filler').value_counts().tolist()[0]
        assert(fillercount == numfillers)
    except IndexError:
        if fillercount == 0:
            return True, {'test': test_name}
        else:
            return False, {
                'test': test_name,
                'class': 'Error',
                'category': 'fillers',
                'message': f'There should be {fillercount} fillers but do not appear to be any'
            }
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'fillers',
            'message': f'There should be {fillercount} fillers but are {numfillers}'
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_condition_counts(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test all conditions occur same number of times as count of critical items."""
    test_name = 'All conditions must occur the same number of times as count of critical items'
    cond_name_counts = df[df['Experiment'] != 'filler'].groupby('Condition').ItemName.nunique()
    itemcount = kwargs.get('itemcount', 999999)
    bad_cond_counts = cond_name_counts[~cond_name_counts.eq(itemcount)]
    try:
        assert(bad_cond_counts.empty)
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'conditions',
            'message': f'Each condition should occur {itemcount} times',
            'problems': [{'Condition': k, 'Count': int(v)} for k, v in sorted(bad_cond_counts.items())]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_no_empty_sentences(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that no sentences are empty strings."""
    test_name = 'Sentences cannot be empty strings'
    empty_sentences = df['Sentence'].isnull()
    try:
        assert(empty_sentences.sum() == 0)
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'sentences',
            'message': f'Sentences may not be empty. You have {empty_sentences.sum()} empty.',
            'problems': {'Empty sentences': [k.item() + 2 for k, v in empty_sentences.items() if v]}
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_sentences_parse(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that all sentences parse."""
    test_name = 'All sentences must be parseable by web SPR input file generator'
    df['GoodSentence'] = df['Sentence'].apply(test_tagged_sentence)
    bad_parses = df.loc[df['GoodSentence'] == False]
    try:
        assert bad_parses.empty
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'sentences',
            'message': 'At least one sentence with region tags has a syntax error',
            'problems': {'Parse failures': [(x, df['Sentence'].loc[x]) for x in bad_parses.index.tolist()]}
        }
    else:
        return True, {'test': test_name}


@TestRegistration.skip
def _test_sentence_ends_punctuation(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that sentences all end with punctuation."""
    test_name = 'All sentences must end with punctuation'
    try:
        assert False
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'message': 'Test is not implemented'
        }
    else:
        return True, {'test': test_name}


@TestRegistration.skip
def _test_no_extra_whitespace(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that sentences contain no extraneous whitespace."""
    test_name = 'Sentences must have exactly one space between each word and no spaces before or after'
    try:
        assert False
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'message': 'Test is not implemented'
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_one_region(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that each sentence contains at least one region."""
    test_name = 'Each critical sentence must have at least one region'
    critdf = df.loc[df['Experiment'] != 'filler']
    tag_counts = critdf['Sentence'].apply(parse_tagged_sentence).apply(lambda x: len(x.tags))
    try:
        assert tag_counts.min() > 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'sentences',
            'message': 'At least one critical sentence has no regions',
            'problems': [{'Row': x + 2, 'Sentence': df['Sentence'].loc[x]} for x in tag_counts.where(lambda x: x == 0).dropna().index.tolist()]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_valid_region_names(df: pd.DataFrame, **kwargs) -> TestResult:
    """
    Test that region names are valid.

    * Cannot end with punctuation, e.g., "{blub}NAME, dsfsd"
    * Must be [A-Za-z0-9]+ FIXME: what is acceptable?
    """
    test_name = 'All region names must consist only of alphanumeric characters and cannot end in punctuation'
    valid_tag = re.compile(r'^[A-Za-z0-9]+$')

    def check_tags(args):
        bad_tags = [tag for tag in args if not re.match(valid_tag, tag)]
        if bad_tags:
            return tuple(bad_tags)
        else:
            return None

    tag_errors = df['Sentence']\
        .apply(parse_tagged_sentence)\
        .apply(lambda x: [t.tag for t in x.tags])\
        .apply(check_tags).dropna().to_dict()

    try:
        assert len(tag_errors) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'sentences',
            'message': 'At least one region tag name is invalid.',
            'problems': [{'Row': k + 2, 'Bad tags': v} for k, v in tag_errors.items()]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_no_empty_answers(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that no questions are empty strings."""
    test_name = 'Questions cannot be the empty string'
    empty_answers = df['Question'].isnull()
    try:
        assert(empty_answers.sum() == 0)
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'question',
            'message': f'Questions may not be empty. You have {empty_answers.sum()} empty.',
            'problems': {'Empty questions': [k.item() + 2 for k, v in empty_answers.items() if v]}
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_answers_yn(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that the only values in 'Answer' are 'Y' and 'N'."""
    test_name = 'The only answer values permitted are Y and N'
    df['Answer'].fillna('[No Value]', inplace=True)
    bad_answers = set(df['Answer'].unique()) - {'Y', 'N'}
    try:
        assert len(bad_answers) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'answers',
            'message': "The only values the 'Answer' column should have are 'Y' or 'N'.",
            'problems': [{'Answer': n, 'Rows': [k.item() + 2 for k, v in df['Answer'].items() if v == n]}
                         for n in bad_answers]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_questions_same_within_item(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that question is the same across an item."""
    test_name = 'Each question should be the same across a given item'
    all_items = df['ItemName'].unique().tolist()
    non_unique_questions = {n: df[df['ItemName'] == n]['Question'].unique().tolist()
                            for n in all_items if df.loc[df['ItemName'] == n]['Question'].nunique() > 1}
    try:
        assert len(non_unique_questions) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'category': 'question',
            'message': 'Questions generally should be the same across a single item.',
            'problems': [{'Item': k, 'Questions': v} for k, v in non_unique_questions.items()]
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_answers_same_within_item(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test that answer is the same across an item."""
    test_name = 'Each answer should be the same across a given item'
    all_items = df['ItemName'].unique().tolist()
    non_unique_answers = [{'Item': n, 'Questions': df[df['ItemName'] == n]['Question'].unique().tolist()}
                          for n in all_items if df.loc[df['ItemName'] == n]['Question'].nunique() > 1]
    try:
        assert len(non_unique_answers) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'category': 'answers',
            'message': 'Answers should be the same across a single item.',
            'problems': non_unique_answers
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_equal_yn(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test equally many Y/N answers within each condition."""
    test_name = 'There should be an equal number of Y/N answers within each condition'
    all_conds = df['Condition'].unique().tolist()
    # count of each answer for each cond
    ans_counts_raw = {n: df[df['Condition'] == n]['Answer'].value_counts() for n in all_conds}
    ans_counts = {k: {k2: v2.item() for k2, v2 in v.items()} for k, v in ans_counts_raw.items()}
    try:
        bad_ans_counts = {k: v for k, v in ans_counts.items() if v['N'] != v['Y']}
        assert len(bad_ans_counts) == 0
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'category': 'answers',
            'message': 'There should be equally many Y/N answers within each condition',
            'problems': [{'Condition': k, 'Counts': v} for k, v in bad_ans_counts.items()]
        }
    except KeyError:  # If there are non {Y,N} keys, then it's already hopelessly screwed up and caught above
        return False, {
            'test': test_name,
            'class': 'Error',
            'category': 'answers',
            'message': "The only values the 'Answer' column should have are 'Y' or 'N' and you must have at least one of each."
        }
    else:
        return True, {'test': test_name}


@TestRegistration.register
def _test_overall_yn_count(df: pd.DataFrame, **kwargs) -> TestResult:
    """Test equally many Y/N answers overall."""
    test_name = 'There should be an equal number of Y/N answers across all items'
    all_yn_counts_raw = df['Answer'].value_counts()
    all_yn_counts = {k: v.item() for k, v in all_yn_counts_raw.items()}
    try:
        assert(all_yn_counts['N'] == all_yn_counts['Y'])
    except AssertionError:
        return False, {
            'test': test_name,
            'class': 'Warning',
            'category': 'answers',
            'message': 'There should be equally many Y/N answers across all stimuli',
            'problems': [{'Counts': all_yn_counts}]
        }
    except KeyError:  # If there are non {Y,N} keys, then it's already hopelessly screwed up and caught above
        return False, {
            'class': 'Error',
            'category': 'answers',
            'message': "The only values the 'Answer' column should have are 'Y' or 'N' and you must have at least one of each."
        }
    else:
        return True, {'test': test_name}

# TODO
# [ ] Check that Condition names are valid_names
# [ ] If "filler" has a different name (e.g. "Filler") things start blowing up in filler checks
# [ ] Check possible ItemNames (currently must be positive integer)
# [ ] if sentence not ending in punctuation
# [ ] if question not ending in question mark
# [ ] if two spaces in a row - does this matter? will be split on whitespace


def validate_excel_spr(filename: str, experiment: str, itemcount: int, fillercount: int, conditions: List[List[str]]) -> Dict[str, str]:
    """Check Excel file of Self Paced Reading stimuli for errors."""
    errors = []
    warnings = []
    passes = []

    df = pd.read_excel(filename)

    # Drop all columns past where 'Answer' should be
    df.drop(df.columns[6:], inplace=True, axis=1)

    for i, test in enumerate(TestRegistration.tests):
        try:
            doespass, result = test(df, experiment=experiment, itemcount=itemcount, fillercount=fillercount, conditions=conditions)
            if doespass:
                passes.append(result)
            else:
                if result['class'] == 'Warning':
                    warnings.append(result)
                else:  # 'Error' and 'Fatal'
                    errors.append(result)
        except Exception as e:
            errors.append({'test': test.__name__, 'category': 'Exception', 'message': e})
            print(f'Uh oh. `{test.__name__}` barfed: {e}', file=sys.stderr)

    return {'errors': errors, 'warnings': warnings, 'passes': passes, 'test_count': len(TestRegistration.tests),
            'pass_count': len(passes), 'error_count': len(errors), 'warning_count': len(warnings)}


if __name__ == '__main__':
    import argparse
    import yaml
    from pprint import pprint

    parser = argparse.ArgumentParser(description='Validate Excel stimuli file')
    parser.add_argument('-f', '--filename', required=True, help='(required) YAML file with info about group')
    args = parser.parse_args()

    with open(args.filename) as yfile:
        group_info = yaml.load(yfile)

    pprint(validate_excel_spr(group_info['filename'],
                              group_info['experiment'],
                              group_info['itemcount'],
                              group_info['fillercount'],
                              group_info['conditions']))
