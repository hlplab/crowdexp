"""Util functions for running HITs on Mechanical Turk."""

import logging
import os
from datetime import datetime, timedelta
# from pprint import pprint
from typing import List

import boto3

from botocore.exceptions import ClientError

from django.conf import settings

from xmltodict import unparse

from ..models import Experiment, MTurkHIT

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'SQS_QUEUE_URL': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

os.environ['AWS_DEFAULT_REGION'] = AWS_SETTINGS['SQS_REGION']
os.environ['AWS_ACCESS_KEY_ID'] = AWS_SETTINGS['AWS_ACCESS_KEY']
os.environ['AWS_SECRET_ACCESS_KEY'] = AWS_SETTINGS['AWS_SECRET_KEY']

logger = logging.getLogger(__name__)


def create_external_question(url: str, height: int) -> str:
    """Create XML for an MTurk ExternalQuestion."""
    return unparse({
        'ExternalQuestion': {
            '@xmlns': 'http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2006-07-14/ExternalQuestion.xsd',
            'ExternalURL': url,
            'FrameHeight': height
        }
    }, full_document=False)


def _get_mturk_connection(sandbox: bool=False):  # returns botocore.client.MTurk, which is created at runtime
    """Get an MTurkConnection, optionally to sandbox."""
    endpoint = 'https://mturk-requester-sandbox.us-east-1.amazonaws.com' if sandbox else 'https://mturk-requester.us-east-1.amazonaws.com'
    return boto3.client('mturk', endpoint_url=endpoint, region_name='us-east-1')  # Currently only us-east-1 has MTurk endpoint


def run_mturk_spr_experiment(experiment: Experiment, experiment_url: str, sandbox: bool=False) -> int:
    """Run SPR MTurk External Question HIT."""
    payment_duration = int(experiment.stimulidesign.design.get('payment_duration', 30))  # FIXME: should we just bail if no payment_duration?
    assignments = 1 if sandbox else (experiment.stimulidesign.design.get('participants', 1))

    qualifications = [
        {
            'QualificationTypeId': '000000000000000000L0',  # PercentAssignmentsApprovedRequirement
            'Comparator': 'GreaterThan',
            'IntegerValues': [95],
            'RequiredToPreview': True
        },
        {
            'QualificationTypeId': '00000000000000000071',  # LocaleRequirement
            'Comparator': 'EqualTo',
            'LocaleValues': [{'Country': 'US'}],
            'RequiredToPreview': True
        }
    ]

    title = 'Self paced reading'
    description = 'Read simple sentences and answer yes/no questions about them.'
    keywords = ','.join(['english', 'language', 'reading', 'psychology', 'experiment'])

    reward = payment_duration / 60 * 6

    duration = int(timedelta(minutes=payment_duration * 1.5).total_seconds()) # `duration` minutes and then give them time and a half to do it
    lifetime = int(timedelta(days=3).total_seconds())
    approvaldelay = int(timedelta(days=15).total_seconds())

    url_params = '?dest=sandbox' if sandbox else '?dest=mturk'
    question = create_external_question(experiment_url + url_params, 680)

    mtc = _get_mturk_connection(sandbox=sandbox)

    response = mtc.create_hit(
            MaxAssignments=assignments,
            AutoApprovalDelayInSeconds=approvaldelay,
            LifetimeInSeconds=lifetime,
            AssignmentDurationInSeconds=duration,
            Reward=f'{reward:.2f}',
            Title=title,
            Keywords=keywords,
            Description=description,
            Question=question,
            RequesterAnnotation='',
            QualificationRequirements=qualifications
        )

    sqs = boto3.resource('sqs')
    try:
        queue = sqs.get_queue_by_name(QueueName=AWS_SETTINGS['SQS_QUEUE'])
    except ClientError:
        queue = None

    created_hit = response.get('HIT', {})

    if created_hit:
        # some of the fields are returned as `datetime` objects which can't be JSON serialized
        for k, v in created_hit.items():
            if isinstance(v, datetime):
                created_hit[k] = v.isoformat()
        new_hit = MTurkHIT(experiment=experiment, props=created_hit, sandbox=sandbox)
        new_hit.save()

        if queue:
            sqs_response = mtc.update_notification_settings(
                HITTypeId=created_hit['HITTypeId'],
                Notification={
                    'Destination': queue.url,
                    'Transport': 'SQS',
                    'Version': '2006-05-05',
                    'EventTypes': [
                        'AssignmentAccepted', 'AssignmentSubmitted', 'AssignmentReturned', 'AssignmentAbandoned',
                        'HITReviewable', 'HITExpired'
                        ]
                },
                Active=True
            )

    return len(created_hit)


def update_hit_info(hits: List[MTurkHIT], sandbox: bool=False) -> None:
    """Update HIT information for MTurkHITs."""
    mtc = _get_mturk_connection(sandbox=sandbox)

    for h in hits:
        try:
            hit = mtc.get_hit(HITId=h.props['HITId']).get('HIT', {})
            if hit:
                # some of the fields are returned as `datetime` objects which can't be JSON serialized
                for k, v in hit.items():
                    if isinstance(v, datetime):
                        hit[k] = v.isoformat()
                h.props.update(hit)
                h.save()
        except ClientError:
            # Most likely the HIT has been deleted on MTurk
            h.props.update({'HIT Status': 'Disposed'})
            h.save()
