from functools import wraps
from typing import Tuple, Dict

TestResult = Tuple[bool, Dict[str, str]]


class TestRegistration:
    """Automatically register classes instead of trying to keep track of a hand built list."""

    tests = []

    @classmethod
    def register(cls, test):
        @wraps(test)
        def wrapper(*args, **kwargs):
            print(f'Registering {test.__name__}')
            return test(*args, **kwargs)
        cls.tests.append(test)
        return wrapper

    @classmethod
    def skip(cls, test):
        @wraps(test)
        def wrapper(*args, **kwargs):
            print(f'Skipping {test.__name__}')
            return test(*args, **kwargs)
        return wrapper
