import os.path
from os import makedirs

from django.conf import settings

from experiment_runner.models import Experiment as RunnerExperiment
from experiment_runner.models import TrialList

from spr_utils.spr_lists import write_spr_files

from ..models import Experiment

ANALYSIS_DIRECTORY = getattr(settings, 'CROWDEXP', None).get('ANALYSIS_DIRECTORY', None)


def generate_lists_for_experiment(experiment: Experiment) -> None:
    """Generate trial list files and TrialList objects for an Experiment."""
    runner_experiment, _ = RunnerExperiment.objects.get_or_create(name=experiment.slug)

    # grab the most recent version of the stimulus file
    excel_file = experiment.folder.children.get(name='Stimuli').files.last()

    flexspr = os.path.join(settings.STATIC_ROOT, 'flexspr')
    if not os.path.exists(flexspr):
        makedirs(flexspr)

    list_names = write_spr_files(experiment_name=experiment.name,
                                 excel_file=excel_file.file,
                                 list_dir=experiment.slug,
                                 base_path=flexspr,
                                 analysis_dir=ANALYSIS_DIRECTORY)
    for lname in list_names:
        TrialList.objects.get_or_create(name=lname, experiment=runner_experiment)
