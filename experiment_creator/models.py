from datetime import date
from hashlib import md5
from os.path import join, splitext
from random import randint
from typing import List

from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount

from ckeditor.fields import RichTextField

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields.jsonb import JSONField
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
from django.utils.text import slugify

from filer.fields.folder import FilerFolderField
from filer.models.filemodels import File

import pandas as pd

from polymorphic.models import PolymorphicModel

from spr_utils.tagged_spr_parser import parse_tagged_sentence

from .util.validate_design import validate_design
from .util.validate_excel_spr import validate_excel_spr

ANALYSIS_DIRECTORY = getattr(settings, 'CROWDEXP', None).get('ANALYSIS_DIRECTORY', None)


def _six_digit_pin():
    return ''.join([str(randint(0, 9)) for _ in range(6)])


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')

    def __str__(self):
        return f"{self.user.username}'s profile"

    class Meta:
        db_table = 'user_profile'

    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def get_display_name(self):
        if self.user.first_name and self.user.last_name:
            return self.user.get_full_name()
        else:
            return self.user.username

    def get_list_name(self):
        if self.user.first_name and self.user.last_name:
            list_name = f'{self.user.last_name}, {self.user.first_name}'
            return list_name.strip()
        else:
            return self.user.username

    def profile_image_url(self, size=75):
        google_uid = SocialAccount.objects.filter(user_id=self.user.id, provider='google')

        if len(google_uid):
            return google_uid[0].get_avatar_url()

        return f"https://www.gravatar.com/avatar/{md5(self.user.email.encode('utf-8')).hexdigest()}?s={size}&d=identicon"

    def profile_url(self):
        return reverse('user_profile', kwargs={'user_id': self.user.id})

    def can_run_experiments(self):
        return self.user.has_perm('experiment_creator.can_run_experiments')


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class Course(models.Model):
    """Course experiments will be run for."""

    TERM_OPTIONS = (
        ('AU', 'Autumn'),
        ('SP', 'Spring'),
        ('SU', 'Summer'),
    )
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    year = models.PositiveSmallIntegerField(
        default=date.today().year,
        validators=[
            RegexValidator(r'^2[0-9]{3}$',
                           ('Enter a valid year. '
                            'Year must be 4 digits, 2xxx'), 'invalid'),
        ],
        help_text='Please enter a 4 digit year, 2xxx'
    )
    term = models.CharField(max_length=2, choices=TERM_OPTIONS)
    current = models.BooleanField(default=True)
    students = models.ManyToManyField(User,
                                      related_name='enrolled_courses',
                                      limit_choices_to={'is_staff': False},
                                      blank=True)
    instructors = models.ManyToManyField(User,
                                         related_name='managed_courses',
                                         limit_choices_to={'is_staff': True},
                                         blank=True)

    def __str__(self):
        return f'{self.term} {self.year} - {self.name}'

    def get_absolute_url(self):
        return reverse('course_dash', kwargs={
            'year': self.year,
            'term': self.term,
            'slug': self.slug,
        })


class CourseGroup(models.Model):
    """Group within a Course."""

    name = models.CharField(max_length=128)
    course = models.ForeignKey(Course)
    joining_pin = models.CharField(max_length=6,
                                   default=_six_digit_pin,
                                   validators=[
                                       RegexValidator(r'^[0-9]{6}$',
                                                      ('Enter a valid PIN. '
                                                       'This value must contain exactly 6 digits'), 'invalid'),
                                   ],
                                   help_text='Please enter a 6 digit PIN')
    # XXX: ideally would limit_choices_to students in that course, something like
    # limit_choices_to=lambda: {'enrolled_courses': course}
    # but there's not an apparent reasonable to implement way to do it (can't refer to course)
    # need to filter in forms
    students = models.ManyToManyField(User,
                                      related_name='course_groups',
                                      blank=True)
    # slug = models.SlugField(max_length=128, unique=True)

    class Meta:
        unique_together = ('name', 'course')

    def __str__(self):
        return 'Group #%d: %s' % (self.id, self.name)

    def get_members(self, **kwargs):
        return self.students.filter(**kwargs)

    def get_absolute_url(self):
        return reverse('group_dash', kwargs={
            'year': self.course.year,
            'term': self.course.term,
            'course_slug': self.course.slug,
            'group_id': self.id,
        })

    def new_random_pin(self):
        """Generate a new group PIN."""
        self.joining_pin = _six_digit_pin()
        self.save()


class Project(models.Model):
    """Project within a Course."""

    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    group = models.ForeignKey(CourseGroup)
    active = models.BooleanField(default=True)

    def __str__(self):
        return 'Project #%d: %s' % (self.id, self.name)

    def get_absolute_url(self):
        return reverse('project_dash', kwargs={'project_id': self.id})


class Experiment(models.Model):
    """The actual experiment."""

    STATUS_OPTIONS = (
        ('DES', 'Proposal and Design'),
        ('STM', 'Stimuli'),
        ('GTS', 'Group Testing'),
        ('STS', 'Staff Testing'),
        ('FAA', 'Final Admin Approval'),
        ('RUN', 'Collecting Data'),
        ('ANL', 'Analyzing Data'),
    )

    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='DES')
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    group = models.ForeignKey(CourseGroup)
    project = models.ForeignKey(Project)
    folder = FilerFolderField(null=True, blank=True, related_name='experiment_folder')

    def __str__(self):
        return 'Experiment #%d: %s' % (self.id, self.name)

    def get_absolute_url(self):
        return reverse('experiment_dash', kwargs={
            'year': self.group.course.year,
            'term': self.group.course.term,
            'course_slug': self.group.course.slug,
            'group_id': self.group.id,
            'slug': self.slug
        })

    def get_anaylsis_dir(self):
        return join(ANALYSIS_DIRECTORY, slugify(self.name))

    class Meta:
        permissions = (('can_run_experiments', 'Can run experiments on MTurk'),)


class ExperimentDocument(PolymorphicModel):
    """Abstract base class for Experiment related documents."""

    group = models.OneToOneField(CourseGroup)
    experiment = models.OneToOneField(Experiment)

    class Meta:
        abstract = True


class StimuliDesign(ExperimentDocument):
    """Experiment proposal and design of Self Paced Reading experiment."""

    STATUS_OPTIONS = (
        ('NEW', 'Not yet specified'),
        ('REQ', 'Approval Pending'),
        ('APP', 'Approved'),
        ('REV', 'Revision Requested by Instructor'),
        ('RUP', 'Revised - Approval not yet requested'),
    )
    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='NEW')
    proposal = RichTextField(default='<p>Describe your proposed experiment here.</p>')
    design = JSONField(default={'manipulations': [], 'trial_count': 0, 'filler_count': 0,
                                'participants': 0, 'group_duration': 30, 'instructor_duration': 30,
                                'payment_duration': 30, 'exname': ''})
    errors = JSONField(default={})

    def __str__(self):
        return 'Design Proposal #%s' % self.id

    def get_absolute_url(self):
        return reverse('proposal-detail', kwargs={'pk': self.id})

    def is_approved(self):
        return self.status == 'APP'

    def get_level_count(self):
        if len(self.design['manipulations']) == 0:
            return '0'
        else:
            return 'x'.join([str(len(x['items'])) for x in self.design['manipulations']])

    def can_download_template(self):
        """
        Only allow download of template if certain criteria are met.

          * Proposal isn't default
          * At least 1 factor with 2 levels
        """
        has_proposal = self.proposal != self.proposal.default
        has_manipulations = self.get_level_count() not in ('0', '1x1')

        return has_proposal and has_manipulations

    def validate(self):
        try:
            self.errors = validate_design(self.design)
            self.save()
        except Exception as err:
            self.errors = {'errors': [{'Exception occurred during validation': str(err)}]}


class ExperimentStimuli(ExperimentDocument):
    """Stimuli for Self Paced Reading experiment in Excel XLSX format."""

    STATUS_OPTIONS = (
        ('NEW', 'Not yet specified'),
        ('UPL', 'Approval not yet requested'),
        ('REQ', 'Approval Pending'),
        ('APP', 'Approved'),
        ('REV', 'Revision Requested by Instructor'),
        ('RUP', 'Revised - Approval not yet requested'),
    )

    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='NEW')
    folder = FilerFolderField(null=True, blank=True, related_name='stimuli_folder')
    errors = JSONField(default={})

    def __str__(self):
        return 'ExperimentStimuli #%s' % self.id

    def get_absolute_url(self):
        return reverse('stimuli-detail', kwargs={'pk': self.id})

    def validate(self):
        """Run stimuli validator on the current uploaded file."""
        self.errors = {}
        self.save()
        try:
            if self.folder and self.folder.file_count > 0:
                design = self.experiment.stimulidesign.design
                manips = [m['items'] for m in design['manipulations']]
                last_stim = self.folder.files.last().file
                self.errors = validate_excel_spr(last_stim,
                                                 design['exname'],
                                                 int(design['trial_count']),
                                                 int(design['filler_count']),
                                                 manips)
                self.save()
        except Exception as e:  # FIXME: what kind of exceptions might happen? catch those and handle specifically
            self.errors = {'errors': [{'category': 'validation', 'message': 'Fatal Validation Error'}], 'warnings': {}}
            self.save()

    def generate_parsed(self) -> List[dict]:
        """
        Parse all the 'Sentences' in the stimulus file.

        Returns a list of dictionaries with of each row from the stimulus file
        with sentence replaced with the output of parse_tagged_sentence
        """
        if self.folder and self.folder.file_count > 0:
            last_stim = self.folder.files.last().file

            df = pd.read_excel(last_stim)
            expected_cols = ['Experiment', 'ItemName', 'Condition', 'Sentence', 'Question', 'Answer']
            if not set(expected_cols).issubset(set(df.columns)):
                return []  # XXX: raising an exception might be more informative
            df = df[expected_cols]
            df['sentence'] = df['Sentence'].apply(parse_tagged_sentence)
            df.rename(columns={'Experiment': 'experiment', 'ItemName': 'item', 'Condition': 'condition',
                               'Question': 'question', 'Answer': 'answer'}, inplace=True)
            df.drop('Sentence', axis=1, inplace=True)
            df['row'] = df.index + 2
            return df.to_dict(orient='records')
        else:
            return []

    class Meta:
        verbose_name_plural = 'stimuli'


class WordDocument(File):
    """Subclass of Django Filer's File model for MS Word documents."""

    file_type = 'MS-Word'
    _icon = 'word'

    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.doc', '.docx', ]
        ext = splitext(iname)[1].lower()
        return ext in filename_extensions


class ExcelDocument(File):
    """Subclass of Django Filer's File model for MS Excel documents."""

    file_type = 'MS-Excel'
    _icon = 'excel'

    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.xls', '.xlsx', ]
        ext = splitext(iname)[1].lower()
        return ext in filename_extensions


class MTurkHIT(models.Model):
    """A HIT object returned by CreateHIT."""

    experiment = models.ForeignKey(Experiment)
    props = JSONField(default={})
    sandbox = models.BooleanField(default=False)
