from glob import glob
from os.path import join
from typing import Union

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils.text import slugify
from django.views.generic import DetailView

from experiment_runner.utils.mturk import fetch_process_notify_hit
from experiment_runner.utils.notifications import process_notifications

from filer.models import Folder

from ..forms import CourseGroupForm
from ..models import Course, CourseGroup, Experiment, ExperimentStimuli, MTurkHIT, Project, StimuliDesign
from ..util import get_files_info
from ..util.mturk import update_hit_info


def university_email_or_staff_check(user: User) -> bool:
    """Check user's email to verify they are a student or staff."""
    # XXX: if a user fails it goes into a horrible redirect loop. How to prevent that?
    # ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = False
    if user.is_anonymous():
        return False
    if user.is_authenticated():
        return user.email.endswith(settings.ALLOWED_ACCOUNT_DOMAINS) or user.is_staff

    return False


@login_required
def user_profile(request: HttpRequest, user_id: int) -> HttpResponse:
    """Display basic user profile page."""
    user = get_object_or_404(User, id=user_id)
    return render(request, 'experiment_creator/user_profile.html', {'user': user})


@user_passes_test(university_email_or_staff_check)
def add_feedback(request: HttpResponse, feedback_type: str, target_id: int) -> HttpResponse:
    """Add top level feedback (comment) to a design or stimuli object."""
    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[feedback_type]

    target = get_object_or_404(target_type, id=target_id)

    return render(request, 'experiment_creator/add_feedback.html', {
        'feedback_target': target,
        'feedback_type': feedback_type,
        'next': request.META.get('HTTP_REFERER', reverse('comments-comment-done'))
    })


@user_passes_test(university_email_or_staff_check)
def create_group(request: HttpRequest, year: int, term: str, course_slug: str) -> Union[HttpResponse, HttpResponseRedirect]:
    """
    Create a CourseGroup for a Course.

    Temporarily staff only, but eventually want students to create their own
    """
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = CourseGroupForm(request.POST, course_id=course.id)
        if form.is_valid():
            group_name = form.cleaned_data['name']
            new_group = CourseGroup(name=group_name, course=course)
            new_group.save()

            group_students = form.cleaned_data['students']
            new_group.students.set(group_students)
            # If a student creates a group, add them to the group even if they didn't add themselves
            if not request.user.is_staff:
                if request.user not in new_group.students.all():
                    new_group.students.add(request.user)
                    messages.add_message(request, messages.INFO, 'Added you to the new group you created')

            # Create related objects that have to exist to be able to do anything

            # Project
            project_name = f'{group_name} Project #1'
            new_project = Project(name=project_name, slug=slugify(project_name), group=new_group)
            new_project.save()

            # Folder
            term_folder, _ = Folder.objects.get_or_create(name=f'{course.term}{course.year}')
            course_folder, _ = term_folder.children.get_or_create(name=course.name)
            project_folder, _ = course_folder.children.get_or_create(name=slugify(group_name))

            # Experiment
            experiment_name = f'{group_name} Experiment #1'
            new_experiment = Experiment(name=experiment_name,
                                        slug=slugify(experiment_name),
                                        group=new_group,
                                        project=new_project,
                                        folder=project_folder)
            new_experiment.save()
            messages.add_message(request, messages.SUCCESS, f'Created {new_group.name}')
            return HttpResponseRedirect(reverse('course_dash',
                                                kwargs={'year': course.year,
                                                        'term': course.term,
                                                        'slug': course.slug}))
    else:
        form = CourseGroupForm(course_id=course.id)

    return render(request, 'experiment_creator/create_group.html', {
        'course': course,
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def get_set_experiment_duration(request: HttpRequest) -> Union[JsonResponse, HttpResponseBadRequest]:
    """
    Get or set the estimated duration of an experiment.

    If user is staff, gets/sets instructor_duration, for normal users gets/sets group_duration
    """
    if request.is_ajax():
        if request.method == 'POST':
            required = {'time_estimate', 'experiment', 'is_staff'}
            post_set = set(request.POST)
            if not required <= post_set:
                return JsonResponse({'error': 'Missing critical keys', 'keys': required - post_set}, status=400)
            try:
                experiment = Experiment.objects.get(slug=request.POST.dict().get('experiment'))
                design = StimuliDesign.objects.get(experiment=experiment)
            except ObjectDoesNotExist as e:
                return JsonResponse({'error': e.__str__()}, status=404)
            else:
                # we want to set group_duration or instructor_duration depending on staff status
                key_to_set = 'instructor_duration' if request.POST.dict().get('is_staff') else 'group_duration'
                design.design[key_to_set] = int(request.POST.dict().get('time_estimate'))
                design.save()
                return JsonResponse({'success': True, key_to_set: design.design[key_to_set]})
        elif request.method == 'GET':
            if 'experiment' not in request.GET:
                return JsonResponse({'error': 'Missing critical key', 'keys': 'experiment'}, status=400)
            try:  # XXX: too much code duplication in this block
                experiment = Experiment.objects.get(slug=request.GET.get('experiment'))
                design = StimuliDesign.objects.get(experiment=experiment)
            except ObjectDoesNotExist as e:
                return JsonResponse({'error': e.__str__()}, status=404)
            else:  # FIXME: this overly optimistically assumes these keys exist
                return JsonResponse({'instructor_duration': design.design['instructor_duration'],
                                     'group_duration': design.design['group_duration']})
    else:
        return HttpResponseBadRequest('Request must be an XHR')


@user_passes_test(university_email_or_staff_check)
def experiment_batches(request: HttpRequest, experiment_id: int) -> HttpResponse:
    """Show MTurk HIT batches for a given experiment."""
    process_notifications()
    experiment = get_object_or_404(Experiment, id=experiment_id)
    batches = MTurkHIT.objects.filter(experiment=experiment)

    mturk_hits = MTurkHIT.objects.filter(experiment=experiment, sandbox=False)
    sandbox_hits = MTurkHIT.objects.filter(experiment=experiment, sandbox=True)

    update_hit_info(mturk_hits)
    update_hit_info(sandbox_hits, sandbox=True)

    results_files = get_files_info(glob(join(experiment.get_anaylsis_dir(), 'results.*.csv')))
    converted_files = get_files_info(glob(join(experiment.get_anaylsis_dir(), 'results_converted*.csv')))

    return render(request, 'experiment_creator/experiment_batches.html', {
        'experiment': experiment,
        'batches': batches,
        'results_files': results_files,
        'converted_files': converted_files,
    })


@user_passes_test(university_email_or_staff_check)
def download_batches(request: HttpRequest, hitid: str) -> JsonResponse:
    """For a given HITId, fetch all batches and process them."""
    fetch_process_notify_hit(hitid)
    return JsonResponse({'download': 'requested'})  # TODO: return something better someday


class ProposalDetailView(DetailView, UserPassesTestMixin):
    # XXX: maybe move DetailView views into own file?
    queryset = StimuliDesign.objects.all()

    def test_func(self):
        return university_email_or_staff_check(self.request.user)

    def get_object(self):
        return super(ProposalDetailView, self).get_object()


class StimuliDetailView(DetailView, UserPassesTestMixin):
    queryset = ExperimentStimuli.objects.all()

    def test_func(self):
        return university_email_or_staff_check(self.request.user)

    def get_object(self):
        return super(StimuliDetailView, self).get_object()
