import logging
from functools import reduce
from operator import mul
from typing import Union

import boto3

from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpRequest, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.utils.text import slugify
from django.views.decorators.http import require_POST

from experiment_runner.models import Experiment as RunnerExperiment

from filer.models import Folder

from ..forms import CourseForm, EmailGroupForm, GroupMemberForm, GroupNonMemberForm
from ..forms import CourseNonStudentsForm, CourseStudentsForm
from ..models import Course, CourseGroup, Experiment, ExperimentStimuli, StimuliDesign
from ..util.contact import email_course_group, email_mturk_runners
from ..util.mturk import run_mturk_spr_experiment

logger = logging.getLogger(__name__)

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

AWS_ACCESS_KEY = AWS_SETTINGS['AWS_ACCESS_KEY']
AWS_SECRET_KEY = AWS_SETTINGS['AWS_SECRET_KEY']


@staff_member_required
def admin_dashboard(request: HttpRequest) -> HttpResponse:
    """Admin dashboard."""
    managed_courses = request.user.managed_courses.all()
    managed_groups = CourseGroup.objects.filter(course__in=managed_courses)

    designs = StimuliDesign.objects.filter(group__in=managed_groups)
    stimuli = ExperimentStimuli.objects.filter(group__in=managed_groups)
    experiments = Experiment.objects.filter(group__in=managed_groups)

    return render(request, 'experiment_creator/admin_dashboard.html', {
        'pending_designs': designs.filter(status='REQ'),
        'rejected_designs': designs.filter(status='REV'),
        'approved_designs': designs.filter(status='APP'),
        'revised_designs': designs.filter(status='RUP'),

        'new_stimuli': stimuli.filter(status__in=('UPL', 'RUP')),
        'pending_stimuli': stimuli.filter(status='REQ'),
        'rejected_stimuli': stimuli.filter(status='REV'),
        'approved_stimuli': stimuli.filter(status='APP'),

        'staff_testing': experiments.filter(status='STS'),
        'final_approval': experiments.filter(status='FAA'),
        'running_experiments': experiments.filter(status='RUN'),
        'analysis_experiments': experiments.filter(status='ANL'),

        'courses': managed_courses,
    })


@staff_member_required
def manage_courses(request: HttpRequest) -> HttpResponse:
    """Staff view to manage courses."""
    current_courses = Course.objects.filter(current=True)
    past_courses = Course.objects.filter(current=False)

    return render(request, 'experiment_creator/manage_courses.html', {
        'current_courses': current_courses,
        'past_courses': past_courses
    })


@staff_member_required
def create_course(request: HttpRequest) -> Union[HttpResponse, HttpResponseRedirect]:
    """Create a new course."""
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = CourseForm(request.POST)
        if form.is_valid():
            course_year = form.cleaned_data['year']
            course_term = form.cleaned_data['term']
            course_name = form.cleaned_data['name']
            course_current = form.cleaned_data['current']
            course_students = form.cleaned_data['students']
            course_instructors = form.cleaned_data['instructors']
            course_slug = slugify(f'{course_name} {course_term} {course_year}')

            new_course = Course(name=course_name, year=course_year, term=course_term,
                                current=course_current, slug=course_slug)
            new_course.save()

            new_course.students.set(course_students)
            new_course.instructors.set(course_instructors)
            new_course.save()

            messages.add_message(request, messages.SUCCESS,
                                 f'Created {new_course.name} ({new_course.term} {new_course.year})')

            term_folder, term_created = Folder.objects.get_or_create(name=f'{new_course.term}{new_course.year}')
            if term_created:
                messages.add_message(request, messages.SUCCESS,
                                     f'Created folder for term: {new_course.term} {new_course.year} at {term_folder.pretty_logical_path}')

            course_folder, course_folder_created = term_folder.children.get_or_create(name=new_course.name)
            if course_folder_created:
                messages.add_message(request, messages.SUCCESS,
                                     f'Created folder for course: {new_course.name} at {term_folder.pretty_logical_path}')

            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseForm()

    return render(request, 'experiment_creator/create_course.html', {
        'form': form,
    })


@staff_member_required
def review_design(request: HttpRequest, design_id: int) -> HttpResponse:
    """Staff view to review experiment design."""
    design = get_object_or_404(StimuliDesign, id=design_id)

    return render(request, 'experiment_creator/design_review.html', {
        'design': design,
    })


@staff_member_required
def review_stimuli(request: HttpRequest, stimuli_id: int) -> HttpResponse:
    """Staff view to review experiment stimuli."""
    stimuli = get_object_or_404(ExperimentStimuli, id=stimuli_id)

    if not stimuli.folder:
        stimuli.folder, _ = stimuli.experiment.folder.children.get_or_create(name='Stimuli')

    return render(request, 'experiment_creator/stimuli_review.html', {
        'stimuli': stimuli,
        'stimuli_folder': stimuli.folder
    })


@staff_member_required
def review_experiment(request: HttpRequest, experiment_id: int) -> HttpResponse:
    """Staff view to review experiment before running."""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    runner_experiment = get_object_or_404(RunnerExperiment, name=experiment.slug)

    return render(request, 'experiment_creator/experiment_review.html', {
        'experiment': experiment,
        'runner_experiment': runner_experiment,
    })


@staff_member_required
def approve_experiment_running(request: HttpRequest, experiment_id: int) -> HttpResponse:
    """Staff view to approve an experiment to run."""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    experiment.status = 'FAA'
    experiment.save()

    subject = 'Your experiment is ready to run'
    body = f'{request.user.get_full_name()} has approved your experiment to run. Data collection will begin soon.'
    email_course_group(experiment.group.id, subject, body)

    messages.add_message(request, messages.INFO, f'Approved "{experiment.name}" for running')

    return HttpResponseRedirect(reverse('prerun_check', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def reject_experiment_running(request: HttpRequest, experiment_id: int) -> HttpResponseRedirect:
    """Staff view to reject a request to run an experiment."""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    # Reset experiment status to editing stimuli
    experiment.status = 'STM'
    experiment.save()

    # Reset stimuli status to revision requested
    stimuli = experiment.experimentstimuli
    stimuli.status = 'REV'
    stimuli.save()

    subject = 'Problems with experiment. Stimuli sent back for revision'
    body = f'{request.user.get_full_name()} found errors with your experiment. Please revise your stimuli.'
    email_course_group(experiment.group.id, subject, body)

    messages.add_message(request, messages.WARNING, f'Sent "{experiment.name}" back for stimuli revision')

    return HttpResponseRedirect(reverse('review_experiment', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def preview_experiment_to_run(request: HttpRequest, experiment_id: int) -> HttpResponse:
    """Staff view to preview an experiment and set price per HIT before running."""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    design = experiment.stimulidesign.design

    participants = int(design.get('participants', 0))
    manipulations = design.get('manipulations', [{'items': []}])
    group_duration = int(design.get('group_duration', None))
    instructor_duration = int(design.get('instructor_duration', None))
    payment_duration = int(design.get('payment_duration', None))

    total_items = int(design.get('trial_count', 0)) + int(design.get('filler_count', 0))

    list_count = reduce(mul, [len(x['items']) for x in manipulations]) * 2

    hit_price = payment_duration / 60 * 6  # $6/hr

    amazon_cut = 0.40 if participants >= 10 else 0.20

    base_hit_cost = participants * hit_price

    total_hit_cost = base_hit_cost * (1 + amazon_cut)

    mtc = boto3.client('mturk',
                       endpoint_url='https://mturk-requester.us-east-1.amazonaws.com',
                       region_name='us-east-1')  # Currently only us-east-1 has MTurk endpoint
    balance = float(mtc.get_account_balance().get('AvailableBalance', 0.0))

    return render(request, 'experiment_creator/experiment_prerun.html', {
        'experiment': experiment,
        'participants': participants,
        'list_count': list_count,
        'total_items': total_items,
        'participants_per_list': participants / list_count,
        'account_balance': balance,
        'group_duration': group_duration,
        'instructor_duration': instructor_duration,
        'payment_duration': payment_duration,
        'hit_price': hit_price,
        'amazon_cut': amazon_cut * 100,
        'base_hit_cost': base_hit_cost,
        'total_hit_cost': total_hit_cost,
        'sufficient_funds': balance > total_hit_cost
    })


@staff_member_required
@require_POST
def set_payment_time(request: HttpRequest, experiment_id: int) -> JsonResponse:
    """Set time to use for MTurk payment."""
    if request.is_ajax():
        try:
            experiment = Experiment.objects.get(id=experiment_id)
            design = StimuliDesign.objects.get(experiment=experiment)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': e.__str__()}, status=404)
        if 'time_for_payment' not in request.POST:
            return JsonResponse({'error': 'Must specify time_for_payment'}, status=400)
        design.design['payment_duration'] = int(request.POST.dict().get('time_for_payment'))
        design.save()
        return JsonResponse({'time_for_payment': design.design['payment_duration']})
    else:
        return HttpResponseBadRequest('Request must be an XHR')


@staff_member_required
def request_final_signoff(request: HttpRequest, experiment_id: int) -> HttpResponseRedirect:
    """Staff view to request final signoff to run from an experiment runner."""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    messages.add_message(request, messages.INFO, 'Requested final sign off to run experiment')

    subject = f'{request.user.get_full_name()} requests to run {experiment.group.name} on MTurk'
    body = f'You can view it for final approval at {reverse("run_on_mturk", kwargs={"experiment_id": experiment.id})}'

    error_string = email_mturk_runners(subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, 'Emailed MTurk runners')

    return HttpResponseRedirect(reverse('prerun_check', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def run_on_mturk(request: HttpRequest, experiment_id: int, sandbox: bool=False) -> HttpResponseRedirect:
    """Staff view for experiment runners to actually run experiment on MTurk."""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    experiment_url = request.build_absolute_uri(reverse('spr_experiment', kwargs={'experiment': experiment.slug}))

    num_hits = run_mturk_spr_experiment(experiment, experiment_url, sandbox)

    if num_hits == 0:
        messages.add_message(request, messages.ERROR, 'Failed to create HITs. See server log.')
    else:
        messages.add_message(request, messages.INFO, f'Created {num_hits} HITs')
        experiment.status = 'RUN'
        experiment.save()

    return HttpResponseRedirect(reverse('experiment_batches', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def toggle_approval(request: HttpRequest, approval_type: str, target_id: int) -> HttpResponseRedirect:
    """Staff view to approve or reject experiment materials."""
    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[approval_type]

    target = get_object_or_404(target_type, id=target_id)

    if target.status == 'APP':
        target.status = 'REV'
    else:
        target.status = 'APP'
    target.save()

    messages.add_message(request, messages.INFO, f'{target} status set to {target.get_status_display()}.')

    subject = f"{request.user.get_full_name()} set {target.group.name}'s {approval_type} to {target.get_status_display()}"
    body = f'You can view the {approval_type} at {request.build_absolute_uri(target.get_absolute_url())}'

    error_string = email_course_group(target.group.id, subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, f'Emailed {target.group.name}')

    return HttpResponseRedirect(reverse(f'review_{approval_type}',
                                        kwargs={f'{approval_type}_id': target.id}))


@staff_member_required
def request_revision(request: HttpRequest, revision_type: str, target_id: int) -> HttpResponseRedirect:
    """Request revision of experiment materials."""
    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[revision_type]

    target = get_object_or_404(target_type, id=target_id)
    target.status = 'REV'
    target.save()

    messages.add_message(request, messages.INFO, f'Requested revision of {revision_type}')

    subject = f'{request.user.get_full_name()} requested {revision_type} revisions from {target.group.name}'
    body = f'You can view the {revision_type} at {request.build_absolute_uri(target.get_absolute_url())}'

    error_string = email_course_group(target.group.id, subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, f'Emailed {target.group.name}')

    return HttpResponseRedirect(reverse(f'review_{revision_type}',
                                        kwargs={f'{revision_type}_id': target.id}))


@staff_member_required
def email_group(request: HttpRequest, group_id: int) -> Union[HttpResponse, HttpResponseRedirect]:
    """Email all members of a group."""
    group = get_object_or_404(CourseGroup, id=group_id)
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = EmailGroupForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            body = form.cleaned_data['body']
            error_string = email_course_group(group_id, subject, body)

            if error_string:
                messages.add_message(request, messages.ERROR, error_string)
            else:
                messages.add_message(request, messages.INFO, f'Emailed {group.name}')

            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = EmailGroupForm()

    return render(request, 'experiment_creator/email_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def manage_group(request: HttpRequest, group_id: int) -> HttpResponse:
    """Manage a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    return render(request, 'experiment_creator/manage_group.html', {
        'group': group
    })


@staff_member_required
def add_members_group(request: HttpRequest, group_id: int) -> Union[HttpResponse, HttpResponseRedirect]:
    """Add members to a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    if request.method == 'POST':
        form = GroupNonMemberForm(request.POST, group_id=group_id)
        if form.is_valid():
            additions = form.cleaned_data['non_members']
            group.students.add(*additions)
            messages.add_message(request, messages.INFO, f'Added {len(additions)} students to {group.name}')
            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = GroupNonMemberForm(group_id=group_id)

    return render(request, 'experiment_creator/add_members_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def remove_members_group(request: HttpRequest, group_id: int) -> Union[HttpResponse, HttpResponseRedirect]:
    """Remove members from a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    if request.method == 'POST':
        form = GroupMemberForm(request.POST, group_id=group_id)
        if form.is_valid():
            removals = form.cleaned_data['members']
            group.students.remove(*removals)
            messages.add_message(request, messages.INFO, f'Removed {len(removals)} students from {group.name}')
            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = GroupMemberForm(group_id=group_id)

    return render(request, 'experiment_creator/remove_members_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def add_students_course(request: HttpRequest, course_id: int) -> Union[HttpResponse, HttpResponseRedirect]:
    """Add students to a course."""
    course = get_object_or_404(Course, id=course_id)

    if request.method == 'POST':
        form = CourseNonStudentsForm(request.POST, course_id=course_id)
        if form.is_valid():
            additions = form.cleaned_data['non_members']
            course.students.add(*additions)
            messages.add_message(request, messages.INFO, f'Added {len(additions)} students to {course.name}')
            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseNonStudentsForm(course_id=course_id)

    return render(request, 'experiment_creator/add_students_course.html', {
        'course': course,
        'form': form
    })


@staff_member_required
def remove_students_course(request: HttpRequest, course_id: int) -> Union[HttpResponse, HttpResponseRedirect]:
    """Remove students from a course."""
    course = get_object_or_404(Course, id=course_id)

    if request.method == 'POST':
        form = CourseStudentsForm(request.POST, course_id=course_id)
        if form.is_valid():
            removals = form.cleaned_data['members']
            course.students.remove(*removals)
            messages.add_message(request, messages.INFO, f'Removed {len(removals)} students from {course.name}')
            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseStudentsForm(course_id=course_id)

    return render(request, 'experiment_creator/remove_students_course.html', {
        'course': course,
        'form': form
    })
