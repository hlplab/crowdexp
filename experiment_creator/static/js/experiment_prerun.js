var vm = new Vue({
  el: '#vue_container',
  data: {
    allow_no_sandbox: false,
    payment_duration: payment_duration,
    account_balance: account_balance,
    participants: participants,
    amazon_cut: amazon_cut,
    setter_url: setter_url,
    dirty_bit: false
  },
  computed: {
    price_per_hit: function() {
      return this.payment_duration / 60 * 6; // $6/hr
    },
    base_hit_cost: function() {
      return this.participants * this.price_per_hit;
    },
    total_hit_cost: function() {
      return this.base_hit_cost * (1 + this.amazon_cut / 100);
    },
    insufficient_funds: function() {
      return this.account_balance < this.total_hit_cost;
    },
    run_disabled: function() {
      return this.insufficient_funds || !this.allow_no_sandbox || this.dirty_bit;
    },
    insufficent_class: function() {
      return {
        'warn': this.insufficient_funds,
        'safe': !this.insufficient_funds
      };
    }
  },
  methods: {
    formatPrice: function(value) {
      var val = (value/1).toFixed(2);
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    },
    setTimeEstimate: _.debounce(function() {
      $.ajax({
        url: vm.setter_url,
        data: {
          time_for_payment: this.payment_duration,
        },
        method: 'POST',
        beforeSend: (xhr, settings) => {
          this.dirty_bit = true;
          xhr.setRequestHeader('X-CSRFToken', Cookies.get('csrftoken'));
        },
      }).done((data, textStatus, jqXHR) => {
        console.log('Time updated: ' + jqXHR.status + ': ' + jqXHR.responseText);
        this.dirty_bit = false;
      }).fail(function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR.status + ' ' + jqXHR.statusText + ': ' + jqXHR.responseText);
      }).always(_.noop());
    }, 750)
  }
});
