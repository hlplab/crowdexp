var vm = new Vue({
  el: '#vue_container',
  data:  {
    boxes: {
      display_correctly: false,
      no_typos: false,
      no_grammatical: false,
      questions_display: false,
      questions_correct: false,
      no_line_breaks: false,
      runs_to_end: false,
      did_demo: false
    }
  },
  computed: {
    all_boxes_checked: function() {
      return _.every(this.boxes);
    }
  }
});
