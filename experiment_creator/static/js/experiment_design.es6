Vue.directive('focus', {
  // https://jsfiddle.net/LukaszWiktor/cap43pdn/
  inserted: function (el) {
    el.focus();
  },
  update: function (el) {
    Vue.nextTick(function() {
      el.focus();
    });
  }
});


Vue.component('vue-alert', {
  props: {
    message: {
      type: String,
      default: '',
      required: true
    },
    type: {
      type: String,
      default: 'warning',
      required: true
    }
  },
  template:
    `<div data-closable v-bind:class="{callout: true, active: true, success: type === 'success', alert: type === 'error', warning: type === 'warning'}">
        {{message}}
        <button v-on:click="$emit('remove')" class="close-button" aria-label="Dismiss alert" type="button" data-close>
          <span aria-hidden="true">&times;</span>
        </button>
    </div>`
});

Vue.component('levels', {
  props: {
    level_name: {
      type: String,
      default: '',
      required: true
    },
    index: {
      type: Number,
      default: 0,
      required: true
    }
  },
  data: function() {
    return {currentlyEditing: false};
  },
  methods: {
    updateFactorName: function() {
      this.currentlyEditing = false;
      vm.$set(this.$parent.levels, this.index, this.$refs.factorinput.value.trim());
    }
  },
  template:
    `<li class='item'>
      <div v-if="currentlyEditing" class='edit-container'>
        <input class='edit' ref='factorinput' v-focus v-bind:value='level_name' v-on:blur='updateFactorName()'>
      </div>
      <div v-else class='view'>
        <label v-on:dblclick='currentlyEditing = true;'>{{level_name}}</label>
        <button v-on:click="$emit('remove-factor')" class='destroy'></button>
      </div>
   </li>`
});

Vue.component('manipulations', {
  props: {
    name: {
      type: String,
      default: '',
      required: true
    },
    levels: {
      type: Array,
      default: [],
      required: true
    },
    index: {
      type: Number,
      default: 0,
      required: true
    }
  },
  data: function() {
    return {currentlyEditing: false}
  },
  methods: {
    addLevelMessage: function(manipname) {
      return 'Add a level to ' + manipname;
    },
    createFactor: function(event) {
      console.log('Creating a new factor on ' + this.name);
      this.levels.push(event.target.value.trim());
      event.target.value = '';
    },
    removeFactor: function(index) {
      console.log('Removing factor ' + this.levels[index] + ' from ' + this.name +' at index ' + index);
      this.levels.splice(index, 1);
    },
    updateManipulationName: function() {
      this.currentlyEditing = false;
      vm.$set(this.$parent.manipulations, this.index, {name: this.$refs.nameinput.value.trim(), items: this.levels});
    },
  },
  template:
    `<li class="item">
      <div v-if="currentlyEditing" class='edit-container'>
        <input class='edit' ref='nameinput' v-focus v-bind:value='name' v-on:blur='updateManipulationName()'>
      </div>
      <div v-else class='view'>
        <label v-on:dblclick='currentlyEditing = true'>{{name}}</label>
        <button v-on:click="$emit('remove-manipulation')" class='destroy'></button>
      </div>
      <div class="levels">
        <h5>Factor levels for manipulation <i>{{name}}</i></h5>
        <p>(these are the condition names that will be used to auto-generate a stimulus
            file; these same names will also be used in the result report we will auto-generate
            for you)
        </p>
      </div>
      <header>
        <input class='new-factor' v-on:change='createFactor($event)' v-bind:placeholder='addLevelMessage(name)'>
      </header>
      <hr/>
      <section class='main'>
        <ul class='factor-list'>
        <levels v-for="(level, idx) in levels"
                v-bind:level_name="level"
                v-bind:key="level"
                v-bind:index="idx"
                v-on:remove-factor="removeFactor(idx)">
        </levels>
        </ul>
      </section>
  </li>`
});

Vue.component('levels-footer', {
  props: {
    trial_count: {
      type: Number,
      default: 0,
      required: true
    },
    filler_count: {
      type: Number,
      default: 0,
      required: true
    },
    participants: {
      type: Number,
      default: 0,
      required: true
    },
    manipulations: {
      type: Array,
      default: function () { return []; },
      required: true
    }
  },
  computed: {
    level_counts: function() {
      return this.manipulations.map(function(e) {return e.items.length;}).join(' x ');
    }
  },
  template:
    `<section class="footer">
      <span class='level-count'>You have a {{level_counts}} level design with {{trial_count}} items and {{filler_count}} 
      fillers per list. You have requested {{participants}} participants.</span>
  </section>`
});

let logArrayElements = (element, index, array) => {
  console.log('a[' + index + '] = ' + element.name + ' : ' + element.items);
};

var vm = new Vue({
  el: '#container',
  data: {
    alerts: [],
    manipulations: [
      // e.g. {name: 'foo', items: ['bar', 'baz']},
    ],
    trial_count: 0,
    filler_count: 0,
    participants: 0,
    exname: '',
  },
  watch: {
    exname: _.debounce(function (newValue, oldValue) {
      if (! _.isUndefined(oldValue)) {
        if (/\s+/.test(newValue)) {
          this.alerts.push( {'message': 'Experiment name should not contain any space or special characters.', 'type': 'error'});
          this.$set(this, 'exname', newValue.trim().replace(/\s+/, '.'));
        }
      }
    }, 500),
    manipulations: {
      handler: function(newValue, oldValue) {
        _.forEach(newValue, logArrayElements);

        // Check that manipulation names are unique (error)
        let manip_names = _.map(this.manipulations, 'name');
        if (! _.isUndefined(manip_names)) {
          let unique_names = _.uniq(manip_names);
          if (manip_names.length > unique_names.length) {
            this.alerts.push( {'message': 'Manipulation names must be unique.', type: 'error'});
          }
        }

        // Check that item names are unique across manipulations (warning)
        let item_names = _.flatten(_.map(this.manipulations, 'items'));
        if (! _.isUndefined(item_names)) {
          let unique_items = _.uniq(item_names);
          if (item_names.length > unique_items.length) {
            this.alerts.push( {'message': 'Level names should be unique even across manipulations.', type: 'warning'});
          }
        }

        // Want to watch 'manipulations.*.items' but Vue doesn't let you watch just nested keys and get keypath
        // // Check that item names are unique within manipulations (error)
        let item_arrays = _.map(this.manipulations, 'items');
        let item_lengths = _.map(item_arrays, function(e) {return e.length;});
        let unique_lengths = _.map(item_arrays, function(e) {return _.uniq(e).length;});
        let item_diff = _.difference(item_lengths, unique_lengths);
        if (! _.isEmpty(item_diff)) {
          this.alerts.push( {'message': 'Level names must be unique within manipulations.', type: 'error'});
        }
      },
      deep: true
    }
  },
  methods: {
    inputKeypress: function() {},
    createManipulation: function(event) {
      console.log('Creating a new manipulation');
      this.manipulations.push({name: event.target.value.trim(), items: []});
      event.target.value = '';
    },
    removeManipulation: function(index) {
      console.log('Removing manipulation at index ' + index);
      this.manipulations.splice(index, 1);
    },
    checkCounts: function() {
      // FIXME: should this be called by a watch method on the vars with a debounce instead of on-blur?
      if (this.trial_count > this.filler_count) {
        this.alerts.push( {'message':
          `It seems like you have more items than fillers. This is not recommended, since it can make it more likely that
           participants will implicitly or explicitly infer the purpose of your experiment. Consider adding more fillers.`,
          'type': 'warning'});
      }
    },
  }
});


let jqxhr = $.getJSON( $('form').attr('action'))
  .done((data, textStatus, jqXHR) => {
    try {
      // TODO: validate that these properties exist and handle if they don't
      vm.trial_count =  _.toNumber(data.design.trial_count);
      vm.filler_count = _.toNumber(data.design.filler_count);
      vm.exname = data.design.exname.trim().replace(/\s+/, '.');
      vm.manipulations = data.design.manipulations;
      vm.participants = _.toNumber(data.design.participants);
      CKEDITOR.instances.proposal.setData(data.proposal);
      console.log( 'successfully loaded data' );
    } catch (e) {
      console.log('Error!: ' + e);
      console.log(data);
    }
  })
  .fail((jqXHR, textStatus, errorThrown) => {
    console.log(jqXHR.status + ' ' +jqXHR.statusText + ': ' + jqXHR.responseText);
    vm.alerts.push({'message': 'Error ' + jqXHR.status + ': ' + jqXHR.responseText, 'type': 'error'});
  })
  .always(() => {
    console.log( 'loading complete' );
  });


$('form').on('submit', (event) => {
  event.preventDefault();

  let form_valid = true;

  let ckcontent = CKEDITOR.instances.proposal.getData();

  let form_data = {manipulations: vm.data.manipulations,
    participants: vm.data.participants,
    trial_count: vm.data.trial_count,
    filler_count: vm.data.filler_count,
    exname: vm.data.exname
  };

  if (!form_data.participants) {
   vm.alerts.push( {'message': '"Number of participants" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.trial_count) {
   vm.alerts.push( {'message': '"Number of critical items" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.filler_count) {
   vm.alerts.push( {'message': '"Number of filler items" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.exname) {
   vm.alerts.push( {'message': '"Experiment Name" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (form_data.manipulations.length === 0) {
   vm.alerts.push( {'message': 'You must have at least one manipulation', 'type': 'error'});
    form_valid = false;
  } else {
    if (_.max(_.map(form_data.manipulations, x => x.items.length)) === 1) {
     vm.alerts.push( {'message': 'At least one manipulation must have two or more levels', 'type': 'error'});
      form_valid = false;
    }
    // TODO: finish writing this validation
  }

  if(form_valid) {
    $.ajax({
      url: $('form').attr('action'),
      data: {
        proposal: ckcontent,
        design: JSON.stringify(form_data)
      },
      method: 'POST',
      beforeSend: (xhr, settings) => {
        xhr.setRequestHeader('X-CSRFToken', $('[name="csrfmiddlewaretoken"]').val());
      },
    }).done((data, textStatus, jqXHR) => {
      console.log('Data uploaded: ' + jqXHR.status + ': ' + jqXHR.responseText);
      $('[type="submit"]').hide();
      $('a[rel="prev"]').removeClass('hidden');
      $('a.button.success').removeClass('disabled');
     vm.alerts.push( {'message': 'Design saved', 'type': 'success'});
    }).fail((jqXHR, textStatus, errorThrown) => {
      console.log(jqXHR.status + ' ' + jqXHR.statusText + ': ' + jqXHR.responseText);
     vm.alerts.push( {'message': 'Error ' + jqXHR.status + ': ' + jqXHR.responseText, 'type': 'error'});
    }).always(_.noop());
  } else {
    return false;
  }
});
