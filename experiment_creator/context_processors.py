from django.conf import settings


def admin_contact(request):
    try:
        primary_admin = settings.ADMINS[0]
    except AttributeError:
        primary_admin = ('No Site Admin Defined', 'test@example.org')
    return {'ADMIN_CONTACT_EMAIL': dict(zip(('name', 'email'), primary_admin))}
