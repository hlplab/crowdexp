from filer.fields.file import FilerFileField

from .models import ExcelDocument, WordDocument


class FilerWordField(FilerFileField):
    default_model_class = WordDocument


class FilerExcelField(FilerFileField):
    default_model_class = ExcelDocument
