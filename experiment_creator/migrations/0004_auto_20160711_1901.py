# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-11 19:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('experiment_creator', '0003_auto_20160617_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentproposal',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_experiment_creator.experimentproposal_set+', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='experimentstimuli',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_experiment_creator.experimentstimuli_set+', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='stimulidesign',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_experiment_creator.stimulidesign_set+', to='contenttypes.ContentType'),
        ),
    ]
